public class VFC_EditBudgetPrestation extends socleEditBudget{
    public VFC_EditBudgetPrestation (ApexPages.StandardController controller) {
        super (controller);
    }
    public override PageReference AddOne() {
        isDisabled = false;
        string annee='';
        integer i = lstbgt.size();
        /*if(i<Annees.size()){
            annee = Annees[i].getValue();
        }*/
        if(i>0){
            Budget__c bLast = lstbgt[i-1];
            Integer iAnnee = 0;
            if(bLast.annee__c!=null){
                iAnnee =Integer.Valueof(bLast.annee__c)+1;
            }
            for(Budget__c b:lstbgt){
                if(Integer.Valueof(b.annee__c)==iAnnee){
                    iAnnee++;                    
                }
            }
            lstbgt.add(new Budget__c(annee__c=''+iAnnee,Statut__c =bLast.Statut__c,reference__c =bLast.reference__c,descriptionPiece__c =bLast.descriptionPiece__c,stateTec__c = 'ADD',name=''+math.random()));
        } else {
            lstbgt.add(new Budget__c(annee__c=Annees[0].getValue(),stateTec__c = 'ADD',name=''+math.random()));
        }

        return null;
    }
    public PageReference UpdateBgt() {
        if(!isvalideEditValue()){
            return null;
        }
        isDisabled = true;
        List<Budget__c> BdtToUp = new List<Budget__c>();
        map<string,Budget__c> mapBudget = new map<string,Budget__c>();
        for(budget__c b:[SELECT id,mois__c,annee__c,ExtID__c from budget__c WHERE opportunity__c=:opr.Id]){
            mapBudget.put(b.ExtID__c,b);
        }
        
        //MAJ DELETE
        boolean isDeletedbudget= false;
        
        List<Budget__c> BdtToDel = FlagTodelete(mapBudget);
        if(BdtToDel.size()>0){
            Database.delete(BdtToDel,true);
            isDeletedbudget = true;
        }
        //MAJ ADD
        BdtToUp.addAll(FlagToAdd(mapBudget));
        //MAJ ADD
        BdtToUp.addAll(FlagToUp(mapBudget));

        if(BdtToUp.size()>0){
            system.debug('### BdtToUp'+BdtToUp);
            Database.UpsertResult[] urs = Database.upsert(BdtToUp,true);
            system.debug('### Database.UpsertResul'+urs );
            for(Database.UpsertResult ur:urs){
                if(!ur.isSuccess()) {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,''+ur.getErrors()[0].getMessage()));
                     break;
                }
            }
        }
        if(BdtToUp.size()>0 || isDeletedbudget){
            refreshBgt();
        }
        return null;
    }
   /* public PageReference AprlBgt() {
        isDisabled = true;
        boolean isSubmitAppRequest = true;
        //MAJ ADD
        for(Budget__c b:FlagToAdd(mapBudget)){
            b.isApproval__c = true;
            b.stateTec__c='ADD';
            BdtToUp.add(b);
        }

        //MAJ DELETE
        for(Budget__c b:FlagTodelete(mapBudget)){
            b.isApproval__c = true;
            b.stateTec__c = 'DEL';
            BdtToUp.add(b);
        }
        List<Budget__c> BdtToUp = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            system.debug('AprlBgt :'+b);
            if(b.stateTec__c=='UP'){
                Budget__c bUp = mapOrigine.get(b.ExtId__c);
                if(isDiff(b,bUp) && b.isApproval__c==false){
                    bUp.stateTec__c = b.stateTec__c;
                    bUp.isApproval__c = true;
                    bUp.apvlAnnee__c = b.annee__c;
                    bUp.ApvlStatut__c = b.statut__c ;
                    bUp.ApvlcaAnnuel__c= b.CaAnnuel__c;
                    bUp.ApvlmbAnnuel__c= b.MbAnnuel__c;
                    BdtToUp.add(bUp);
                }
            } else if(b.stateTec__c=='ADD'){
                for (String mois : lstMois) {
                    Budget__c bn = b.clone(false,false,false,false);
                    bn.isApproval__c = true;
                    bn.Opportunity__c = oprId;
                    bn.mois__c = mois;
                    CalcMensuel(bn,(decimal)Taux.get(mois+'_mth__c'));
                    BdtToUp.add(bn);   
                }
            } else if(b.stateTec__c=='DEL'){
                b.isApproval__c = true;
                BdtToUp.add(b);
            }
        }
        if(BdtToUp.size()>0){
            Database.UpsertResult[] urs = Database.upsert(BdtToUp , Budget__c.Fields.ExtID__c,false);
            for(Database.UpsertResult ur:urs){
                if(!ur.isSuccess()) {
                    isSubmitAppRequest = false;
                    isDisabled = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,''+ur.getErrors()[0].getMessage()));
                    break;
                }
            }
            refreshBgt();
        }
        if(isSubmitAppRequest ){
            Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
            req1.setObjectId(oprid);
            req1.setComments('Merci de valider la modification des budgets.');
            req1.setProcessDefinitionNameOrId('ApvlBudget');
            req1.setSkipEntryCriteria(true);
            Approval.ProcessResult result = Approval.process(req1);
            refreshOpr();
        }
        return null;
    }*/
    public PageReference AprlBgt() {
        if(!isvalideEditValue()){
            return null;
        }
        calc();
        boolean isSubmitAppRequest = true;
        isDisabled = true;
        map<string,Budget__c> mapBudget = new map<string,Budget__c>();
        for(budget__c b:[SELECT id,mois__c,annee__c,ExtID__c from budget__c WHERE opportunity__c=:oprId]){
            mapBudget.put(b.ExtID__c,b);
        }
        List<Budget__c> BdtToUp = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            system.debug('AprlBgt :'+b);
            if(b.stateTec__c=='UP'){
                // is submit for update
                Budget__c bo = mapOrigine.get(b.ExtId__c);
                bo.stateTec__c = b.stateTec__c;
                bo.isApproval__c = true;
                bo.apvlAnnee__c = b.annee__c;
                bo.ApvlStatut__c = b.statut__c ;
                bo.ApvlcaAnnuel__c= b.CaAnnuel__c;
                bo.ApvlmbAnnuel__c= b.MbAnnuel__c;
                bo.ApvldescriptionPiece__c= b.descriptionPiece__c;
                bo.ApvlVolumeAnnuelle__c= b.volumeAnnee__c ;
                bo.apvlReference__c = b.Reference__c;
                bo.ApvlprixPieceBrut__c= b.prixPieceBrut__c;
                bo.ApvlprixPieceNet__c  = b.prixPieceNet__c;
                bo.ApvlTauxCommission__c  = b.tauxCommission__c;
                bo.ApvlcaBrutAnnuel__c = b.caBrutAnnuel__c;
                bo.ApvlcaNetAnnuel__c = b.caNetAnnuel__c;
                for (String mois : lstMois) {
                    Budget__c bUp = bo.clone(false,false,false,false);
                    string ExtID = genExtId(bO,mois);
                    system.debug('ExtID :'+ExtID);
                    bUp.id = mapBudget.get(ExtID).id;
                    bUp.mois__c = mois;
                    bUp.ExtID__c = ExtID;
                    bUp.stateTec__c='UP';
                    BdtToUp.add(bUp);
                }
            }
        }
        //MAJ ADD
        for(Budget__c b:FlagToAdd(mapBudget)){
            b.isApproval__c = true;
            b.stateTec__c='ADD';
            BdtToUp.add(b);
        }

        //MAJ DELETE
        for(Budget__c b:FlagTodelete(mapBudget)){
            b.isApproval__c = true;
            b.stateTec__c = 'DEL';
            BdtToUp.add(b);
        }
        if(BdtToUp.size()>0){
            Database.UpsertResult[] urs = Database.upsert(BdtToUp,true);
            for(Database.UpsertResult ur:urs){
                if(!ur.isSuccess()) {
                    isSubmitAppRequest = false;
                    isDisabled = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,''+ur.getErrors()[0].getMessage()));
                    break;
                }
            }
            refreshBgt();
        }
        if(isSubmitAppRequest ){
            Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
            req1.setObjectId(oprid);
            req1.setComments('Merci de valider la modification des budgets.');
            req1.setProcessDefinitionNameOrId('ApvlBudget');
            req1.setSkipEntryCriteria(true);
            Approval.ProcessResult result = Approval.process(req1);
            refreshOpr();
        }
        return null;
    }
    boolean isvalideEditValue(){
        boolean IsOk =true;
        for(budget__c b:lstbgt){
            if( string.isEmpty(b.annee__c) || 
                b.CaAnnuel__c== null || 
                b.MbAnnuel__c== null || 
                //string.isEmpty(b.annee__c) ||
                string.isEmpty(b.statut__c)){
                IsOk = false;
                break;
            }
        }
        if(!IsOk){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.ValidationField));
        }
        return IsOk; 
    }
}