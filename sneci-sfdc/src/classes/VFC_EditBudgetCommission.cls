public class VFC_EditBudgetCommission extends socleEditBudget {
    public integer sizeGlobal {
        private set;
        get{
            return lstGlobalBdg.size();
        }
    }
    public boolean isDetail {set;get;}
    public VFC_EditBudgetCommission (ApexPages.StandardController controller) {
        super (controller);
        isDetail=false;
    }
    
    public PageReference setMode() {
        isDetail=!isDetail;
        return null;
    }
    public PageReference UpdateBgt() {
        if(!isvalideEditValue()){
            return null;
        }
        isDisabled = true;
        List<Budget__c> BdtToUp = new List<Budget__c>();
        
        map<string,Budget__c> mapBudget = new map<string,Budget__c>();
        for(budget__c b:[SELECT id,mois__c,annee__c,ExtID__c from budget__c WHERE opportunity__c=:oprId]){
            mapBudget.put(b.ExtID__c,b);
        }
        boolean isDeletedbudget = false;
        //MAJ DELETE
        List<Budget__c> BdtToDel = FlagTodelete(mapBudget);
        if(BdtToDel.size()>0){
            delete BdtToDel;
            isDeletedbudget = true;
        }
        //MAJ ADD
        BdtToUp.addAll(FlagToAdd(mapBudget));
        // Update
        for(Budget__c b:lstbgt) {
            system.debug('### annee1 '+b.annee__c);
            Budget__c bO = mapOrigine.get(b.ExtID__c);
            string anneeOrigine =(bO!=null)?bO.annee__c:b.annee__c;
            system.debug('### b='+b);
            if(b.stateTec__c == 'UP'){
                string ReferenceAPI = b.Reference__c+math.random();
                for (String mois : lstMois) {
                    Budget__c bn = b.clone(false,false,false,false);
                    system.debug('### bn0='+bn);
                    system.debug('### opr='+opr);
                    system.debug('### oprid='+oprid);
                    bn.id = mapBudget.get(genExtId(bO,mois)).id;
                    system.debug('### bn1='+b.opportunity__r.numeroOpr__c);
                    //bn.opportunity__r = opr;
                    //bn.opportunity__c = oprid;
                    bn.ReferenceAPI__c = ReferenceAPI;
                    bn.mois__c = mois;
                    CalcMensuel(bn,(decimal)Taux.get(mois+'_mth__c'));
                    system.debug('### bn3='+bn);
                    bn.stateTec__c=null;
                    BdtToUp.add(bn); 
                }
            }
        }
        if(BdtToUp.size()>0){
            system.debug('### BdtToUp'+BdtToUp);
            Database.UpsertResult[] urs = Database.upsert(BdtToUp,false);
            system.debug('### Database.UpsertResul'+urs );
            for(Database.UpsertResult ur:urs){
                if(!ur.isSuccess()) {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,''+ur.getErrors()[0].getMessage()));
                     break;
                }
            }
        }
        if(BdtToUp.size()>0 || isDeletedbudget){
            refreshBgt();
        }
        return null;
    }
    public PageReference AprlBgt() {
        if(!isvalideEditValue()){
            return null;
        }
        calc();
        boolean isSubmitAppRequest = true;
        isDisabled = true;
        map<string,Budget__c> mapBudget = new map<string,Budget__c>();
        for(budget__c b:[SELECT id,mois__c,annee__c,ExtID__c from budget__c WHERE opportunity__c=:oprId]){
            mapBudget.put(b.ExtID__c,b);
        }
        List<Budget__c> BdtToUp = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            system.debug('AprlBgt :'+b);
            if(b.stateTec__c=='UP'){
                // is submit for update
                Budget__c bo = mapOrigine.get(b.ExtId__c);
                bo.stateTec__c = b.stateTec__c;
                bo.isApproval__c = true;
                bo.apvlAnnee__c = b.annee__c;
                bo.ApvlStatut__c = b.statut__c ;
                bo.ApvlcaAnnuel__c= b.CaAnnuel__c;
                bo.ApvlmbAnnuel__c= b.MbAnnuel__c;
                bo.ApvldescriptionPiece__c= b.descriptionPiece__c;
                bo.ApvlVolumeAnnuelle__c= b.volumeAnnee__c ;
                bo.apvlReference__c = b.Reference__c;
                bo.ApvlprixPieceBrut__c= b.prixPieceBrut__c;
                bo.ApvlprixPieceNet__c  = b.prixPieceNet__c;
                bo.ApvlTauxCommission__c  = b.tauxCommission__c;
                bo.ApvlcaBrutAnnuel__c = b.caBrutAnnuel__c;
                bo.ApvlcaNetAnnuel__c = b.caNetAnnuel__c;
                for (String mois : lstMois) {
                    Budget__c bUp = bo.clone(false,false,false,false);
                    string ExtID = genExtId(bO,mois);
                    system.debug('ExtID :'+ExtID);
                    bUp.id = mapBudget.get(ExtID).id;
                    bUp.mois__c = mois;
                    bUp.ExtID__c = ExtID;
                    bUp.stateTec__c='UP';
                    BdtToUp.add(bUp);
                }
            }
        }
        //MAJ ADD
        for(Budget__c b:FlagToAdd(mapBudget)){
            b.isApproval__c = true;
            b.stateTec__c='ADD';
            BdtToUp.add(b);
        }

        //MAJ DELETE
        for(Budget__c b:FlagTodelete(mapBudget)){
            b.isApproval__c = true;
            b.stateTec__c = 'DEL';
            BdtToUp.add(b);
        }
        if(BdtToUp.size()>0){
            Database.UpsertResult[] urs = Database.upsert(BdtToUp,true);
            for(Database.UpsertResult ur:urs){
                if(!ur.isSuccess()) {
                    isSubmitAppRequest = false;
                    isDisabled = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,''+ur.getErrors()[0].getMessage()));
                    break;
                }
            }
            refreshBgt();
        }
        if(isSubmitAppRequest ){
            Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
            req1.setObjectId(oprid);
            req1.setComments('Merci de valider la modification des budgets.');
            req1.setProcessDefinitionNameOrId('ApvlBudget');
            req1.setSkipEntryCriteria(true);
            Approval.ProcessResult result = Approval.process(req1);
            refreshOpr();
        }
        return null;
    }
    boolean isvalideEditValue(){
        boolean IsOk =true;
        for(budget__c b:lstbgt){
            if( string.isEmpty(b.reference__c) || 
                b.volumeAnnee__c == null || 
                b.prixPieceBrut__c== null || 
                b.prixPieceNet__c== null || 
                b.tauxCommission__c== null||
                //string.isEmpty(b.annee__c) ||
                string.isEmpty(b.reference__c)){
                system.debug('###'+b);
                IsOk = false;
                break;
            }
        }
        
        if(!IsOk){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.ValidationField));
        }
        return IsOk; 
    }
    
    /*
    private void refreshBgt() {
        if(opr !=null){
            mapOrigine.clear();
            lstbgt.clear();
            lstBdgTete.clear();
            for(Budget__c b:[select annee__c,mois__c,isApproval__c,ExtID__c,descriptionPiece__c,referenceAPI__c

                        volumeAnnee__c,Reference__c,tauxCommission__c,caAnnuel__c,mbAnnuel__c,prixPieceBrut__c,prixPieceNet__c ,caNetAnnuel__c,caBrutAnnuel__c,Statut__c,opportunity__r.numeroOpr__c,
                        ApvlVolumeAnnuelle__c,apvlReference__c,ApvlTauxCommission__c,ApvlCaAnnuel__c,ApvlMbAnnuel__c,ApvlPrixPieceBrut__c,ApvlPrixPieceNet__c,ApvlStatut__c FROM Budget__c where opportunity__c =:opr.id AND mois__c='JANVIER' ORDER BY Reference__c,annee__c asc]){
                mapOrigine.put(b.ExtID__c,b.clone(false,false,false,false)); 
                lstbgt.add(b);
                if(b.isApproval__c){
                    Budget__c bApp = b.clone(false,false,false,false);
                    b.isApproval__c = false;
                    bApp.Statut__c = bApp.ApvlStatut__c ;
                    bApp.Reference__c = bApp.apvlReference__c;
                    bApp.volumeAnnee__c = bApp.ApvlVolumeAnnuelle__c;
                    bApp.prixPieceBrut__c = bApp.ApvlPrixPieceBrut__c;
                    bApp.prixPieceNet__c = bApp.ApvlPrixPieceNet__c;
                    bApp.tauxCommission__c = bApp.ApvlTauxCommission__c;
                    bApp.caAnnuel__c = bApp.ApvlCaAnnuel__c;
                    bApp.mbAnnuel__c = bApp.ApvlMbAnnuel__c;
                    lstbgt.add(bApp);
                }
                if(!isBusinessDeveloppement ){
                    for(SelectOption so:Annees){
                        if(b.annee__c==so.getValue()){
                            so.setDisabled(true);
                        }
                    }
                }
            }
            calc();
            for(AggregateResult ar:[select Annee__c,Statut__c,SUM(caAnnuel__c) sumCA,SUM(caBrutAnnuel__c) sumCABrut,SUM(caNetAnnuel__c) sumCANet
                                    FROM Budget__c where opportunity__c =:opr.id
                                    AND mois__c ='JANVIER' 
                                    group by Annee__c,Statut__c ORDER BY Annee__c]){
                 Budget__c bt = new Budget__c ();
                 bt.Annee__c = (string)ar.get('Annee__c');
                 bt.Statut__c = (string)ar.get('Statut__c');
                 bt.caAnnuel__c = (decimal)ar.get('sumCA');
                 bt.caBrutAnnuel__c = (decimal)ar.get('sumCABrut');
                 bt.caNetAnnuel__c = (decimal)ar.get('sumCANet');
                 for(Budget__c b:lstbgt){
                     if(bt.Annee__c == b.Annee__c && b.isApproval__c){
                         bt.isApproval__c = true;
                         break;
                     }
                 }
                 lstBdgTete.add(bt);
            }    
        }
    }   
*/
    /*
    private void calc() {
        for(Budget__c bgt:lstbgt ){
            if(bgt.prixPieceBrut__c!=null && bgt.volumeAnnee__c!=null){
                bgt.caBrutAnnuel__c= bgt.prixPieceBrut__c*bgt.volumeAnnee__c;
            }
            if(bgt.prixPieceNet__c!=null && bgt.volumeAnnee__c!=null){
                bgt.caNetAnnuel__c  = bgt.prixPieceNet__c*bgt.volumeAnnee__c;
            }
            if(bgt.prixPieceNet__c!=null && bgt.volumeAnnee__c!=null && bgt.tauxCommission__c!=null){
                bgt.caAnnuel__c = bgt.tauxCommission__c*bgt.prixPieceNet__c*bgt.volumeAnnee__c/100;
                bgt.mbAnnuel__c = bgt.caAnnuel__c;
            }     
        }
    }*/
    /*
    public PageReference UpdateBgt() {
        isDisabled = true;
        List<Budget__c> BdtToUp = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            quotaBudget__mdt quotas = mapQuota.get(opr.categorieService__c);
            if(quotas == null){
               quotas = mapQuota.get('DEFAULT');
            }
            if(b.opportunity__c==null){
                b.opportunity__c = opr.id;
            }
            Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.quotaBudget__mdt.fields.getMap();
            for (String fieldName : schemaFieldMap.keySet()) {
                if(fieldName.endswith('_mth__c')){
                    Budget__c bn = b.clone(false,false,false,false);
                    BdtToUp.add(bn);
                    decimal quota = (decimal)quotas.get(fieldName);
                    bn.mois__c = fieldName.replace('_mth__c','');
                    if(bn.prixPieceBrut__c!=null && bn.volumeAnnee__c!=null){
                        bn.caBrutAnnuel__c= bn.prixPieceBrut__c*bn.volumeAnnee__c;
                    }
                    if(bn.prixPieceNet__c!=null && bn.volumeAnnee__c!=null){
                        bn.caNetAnnuel__c  = bn.prixPieceNet__c*bn.volumeAnnee__c;
                    }
                    if(bn.prixPieceNet__c!=null && bn.volumeAnnee__c!=null && bn.tauxCommission__c!=null){
                        bn.caAnnuel__c = bn.tauxCommission__c*bn.prixPieceNet__c*bn.volumeAnnee__c/100;
                        bn.mbAnnuel__c = bn.caAnnuel__c;
                    }  
                    bn.caMensuel__c = quota*bn.caAnnuel__c/100;
                    bn.mbMensuel__c = quota*bn.MbAnnuel__c/100;
                    if(bn.volumeAnnee__c!=null){
                        bn.volumeMensuel__c = quota*bn.volumeAnnee__c/100;
                    }                        
                    bn.ExtID__c = ''+bn.annee__c+bn.mois__c+opr.numeroOpr__c+(''+bn.referenceAPI__c).replace('null','');
                    bn.Name = ''+bn.annee__c+' '+bn.mois__c;
                }
            }
        }
        if(BdtToUp.size()>0){
            Database.UpsertResult[] urs = Database.upsert(BdtToUp , Budget__c.Fields.ExtID__c, false);
            for(Database.UpsertResult ur:urs){
                if(!ur.isSuccess()) {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,''+ur.getErrors()));
                }
            }
            refreshBgt();
        }
        return null;
    }*/
    protected override void refreshBgt() {
        super.refreshBgt();
        refreshGlobalBgt();
    }
    private void refreshGlobalBgt() {
        if(opr !=null){
            lstGlobalBdg.clear();
            for(AggregateResult ar:[select Annee__c,Statut__c,SUM(caAnnuel__c) sumCA,SUM(caBrutAnnuel__c) sumCABrut,SUM(caNetAnnuel__c) sumCANet
                                    FROM Budget__c 
                                    where opportunity__c =:opr.id AND mois__c ='JANVIER' 
                                    group by Annee__c,Statut__c ORDER BY Annee__c]){
                 Budget__c bt = new Budget__c ();
                 bt.Annee__c = (string)ar.get('Annee__c');
                 bt.Statut__c = (string)ar.get('Statut__c');
                 bt.caAnnuel__c = (decimal)ar.get('sumCA');
                 bt.caBrutAnnuel__c = (decimal)ar.get('sumCABrut');
                 bt.caNetAnnuel__c = (decimal)ar.get('sumCANet');
                 for(Budget__c b:lstbgt){
                     if(bt.Annee__c == b.Annee__c && b.isApproval__c){
                         bt.isApproval__c = true;
                         break;
                     }
                 }
                 lstGlobalBdg.add(bt);
            }
        }   
    }
}