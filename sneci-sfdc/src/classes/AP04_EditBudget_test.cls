@isTest
private class AP04_EditBudget_test {
    
    //Approve or Reject l'approbation
    static void doApproval(Id oprID,string code,boolean isApproved) {
        ProcessInstanceWorkitem[] piwis = [SELECT Id,ProcessInstance.ProcessDefinition.DeveloperName FROM ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =:oprID];
        if(piwis.size()>0){
            ProcessInstanceWorkitem piwi = piwis[0];
            system.debug('##[A]'+piwi.ProcessInstance.ProcessDefinition.DeveloperName);
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setAction(isApproved?'Approve':'Reject');
            req.setNextApproverIds(new id[]{UserInfo.getUserId()});
            req.setWorkitemId(piwi.id);
            req.setComments(code);
            Approval.ProcessResult result = Approval.process(req);
        }
    }
    
    //declencher l'approbation
    static void sendApproval(Id oprID) {
        Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
        req1.setObjectId(oprID);
        req1.setComments('Merci de valider la modification des budgets.');
        req1.setProcessDefinitionNameOrId('ApvlBudget');
        req1.setSkipEntryCriteria(true);
        Approval.ProcessResult result = Approval.process(req1);
    }
    static List<Budget__c> creatinBdg(Opportunity op){
        List<Budget__c> bdg = new List<Budget__c>();
        for(string mois :socleEditBudget.lstMois){
            bdg.add(new Budget__c(Name = 'loulou', annee__c = '2016', mois__c = mois, isApproval__c = true, Reference__c = 'test',Statetec__c='ADD',opportunity__c=op.Id,
                                volumeAnnee__c = 12.000, caMensuel__c = 7.000, mbMensuel__c = 5.000,mbAnnuel__c = 9.000,  ExtId__c = '2016'+mois+op.numeroOpr__c));
            bdg.add(new Budget__c(Name = 'loulou', annee__c = '2017', mois__c = mois, isApproval__c = true, Reference__c = 'test',Statetec__c='DEL',opportunity__c=op.Id,
                                volumeAnnee__c = 12.000, caMensuel__c = 7.000, mbMensuel__c = 5.000,mbAnnuel__c = 9.000,  ExtId__c = '2017'+mois+op.numeroOpr__c));
            bdg.add(new Budget__c(Name = 'loulou', annee__c = '2018', mois__c = mois, isApproval__c = true, Reference__c = 'test',Statetec__c='UP',opportunity__c=op.Id,volumeAnnee__c = 12.000,mbAnnuel__c = 9.000,ExtId__c = '2018'+mois+op.numeroOpr__c,Statut__c='A RISQUE',descriptionPiece__c='test descriptionPiece',CaAnnuel__c = 125.000,
                                 ApvlVolumeAnnuelle__c = 50.000, ApvlReference__c = 'lala', Apvlannee__c='2020', ApvldescriptionPiece__c='test APVL descriptionPiece', ApvlcaBrutAnnuel__c=100.00, ApvlcaNetAnnuel__c=57.50, ApvlStatut__c='POTENTIEL',
                                 ApvlCaAnnuel__c = 1.150,  ApvlMbAnnuel__c = 1.15, ApvlprixPieceBrut__c = 2.000, ApvlprixPieceNet__c = 1.15, ApvlTauxCommission__c = 2.00));
            bdg.add(new Budget__c(Name = 'loulou', annee__c = '2019', mois__c = mois, isApproval__c = true, Reference__c = 'test',opportunity__c=op.Id,
                                volumeAnnee__c = 12.000, caMensuel__c = 7.000, mbMensuel__c = 5.000,mbAnnuel__c = 9.000,  ExtId__c = '2019'+mois+op.numeroOpr__c));
        }                                                                                      
        insert bdg;
        return bdg;
    }
    @isTest static void TestEditHandlerApproved(){
        SendTest(true);
    }
    @isTest static void TestEditHandlerRejected() {
        SendTest(false);
    }    
    static void SendTest(boolean isApproved) {
    //data initialisation
        Opportunity op =new Opportunity (validationContrat__c = 'Approuvée', Responsable_BU__c = UserInfo.getUserId(), Name = 'opp1', CloseDate = Date.Today(),numeroOpr__c='Opr1', StageName = 'CREATION OPR',Bureau_SNECI_contractant__c = 'SNECI France');
        insert op ;
        List<Budget__c> bdgs = creatinBdg(op);
        op.StageName=Label.SuiviOpr;
        op.histoEtape__c=Label.SuiviOpr;
        update op ;
        
        sendApproval(op.Id);

        
        test.startTest();
        doApproval(op.Id,'',isApproved);
        test.stopTest();
        AssertBdg(isApproved,bdgs,op.Id);
    }
    
    static Void AssertBdg(boolean isApproved,List<Budget__c> bdgs,Id oprId){
        map<id,Budget__c> mapBdg = new map<id,Budget__c>([select id,Name,mois__c, isApproval__c ,Statetec__c,ExtId__c,ReferenceAPI__c,
                                                          annee__c,Statut__c,Reference__c,volumeAnnee__c,caAnnuel__c,mbAnnuel__c, TauxCommission__c,prixPieceBrut__c,prixPieceNet__c,descriptionPiece__c,caBrutAnnuel__c,caNetAnnuel__c,
                                                          Apvlannee__c,ApvlStatut__c,ApvlReference__c,ApvlVolumeAnnuelle__c,ApvlCaAnnuel__c,ApvlMbAnnuel__c,ApvlprixPieceBrut__c, ApvlprixPieceNet__c, ApvlTauxCommission__c,ApvldescriptionPiece__c,ApvlcaBrutAnnuel__c,ApvlcaNetAnnuel__c
                                                          from Budget__c where opportunity__c =:oprId]);
        
        for(Budget__c b:bdgs){
            Budget__c Btmp = mapBdg.get(b.id);
                
            if(isApproved){
                if(b.Statetec__c=='UP'){
                    //system.Assert(false,b+' ##########'+Btmp);
                    CheckSup(Btmp,false,'Approved update must not Be DEL');
					isSameApvl(b,Btmp,'Approved update ');
                } else if(b.Statetec__c=='ADD'){
                    CheckSup(Btmp,false,'Approved creation must not Be DEL');
                     isSame(b,Btmp,'Approved creation ');
                } else if(b.Statetec__c=='DEL'){
                    CheckSup(Btmp,true,'Approved Suppression must Be DEL'); 
                } else {
                    CheckSup(Btmp,false,'Approved normal must not Be DEL');
                    isSame(b,Btmp,'Approved normal ');
                }
            } else {
                if(b.Statetec__c=='UP'){
                    CheckSup(Btmp,false,'Rejected update must not Be DEL');
                    isSame(b,Btmp,'Rejected update ');
                } else if(b.Statetec__c=='ADD'){
                    CheckSup(Btmp,true,'Rejected creation must Be DEL'); 
                } else if(b.Statetec__c=='DEL'){
                    CheckSup(Btmp,false,'Rejected Suppression must not Be DEL');
                    isSame(b,Btmp,'Rejected Suppression ');
                } else {
                    CheckSup(Btmp,false,'Rejected normal must not Be DEL');
                    isSame(b,Btmp,'Rejected normal ');
                }
            }
        }
    }
    static void Check(Budget__c b1,string champ1,Budget__c b2,string champ2,boolean bOk,string Context) {
        boolean Result =false;
        if(b1.get(champ1)==b2.get(champ2)){
            Result = true;
        }
        system.Assert(Result==bOk,Context+' Bgt.'+champ1+'=['+b1.get(champ1)+'] BgtUP.'+champ2+'=['+b2.get(champ2)+'] => ['+Result+'] But must be ['+bOk+']');
    }
    static void CheckSup(Budget__c b1,boolean bOk,string Context) {
        boolean Result =false;
        if(b1==null){
            Result = true;
        }
        system.Assert(Result==bOk,Context+' => ['+Result+'] But must be ['+bOk+']');
    }
    static void isSame(Budget__c b,Budget__c Btmp,string Context) {
        Check(b,'annee__c',Btmp,'annee__c',true,Context);
        Check(b,'Statut__c',Btmp,'Statut__c',true,Context);
        Check(b,'Reference__c',Btmp,'Reference__c',true,Context);
        Check(b,'volumeAnnee__c',Btmp,'volumeAnnee__c',true,Context);
        Check(b,'caAnnuel__c',Btmp,'caAnnuel__c',true,Context);
        Check(b,'mbAnnuel__c',Btmp,'mbAnnuel__c',true,Context);
        Check(b,'TauxCommission__c',Btmp,'TauxCommission__c',true,Context);
        Check(b,'prixPieceBrut__c',Btmp,'prixPieceBrut__c',true,Context);
        Check(b,'prixPieceNet__c',Btmp,'prixPieceNet__c',true,Context);
        Check(b,'descriptionPiece__c',Btmp,'descriptionPiece__c',true,Context);
        Check(b,'caBrutAnnuel__c',Btmp,'caBrutAnnuel__c',true,Context);
        Check(b,'caNetAnnuel__c',Btmp,'caNetAnnuel__c',true,Context);
    }
    static void isSameApvl(Budget__c b,Budget__c Btmp,string Context){ 
        Check(b,'Apvlannee__c',Btmp,'annee__c',true,Context);
        Check(b,'ApvlStatut__c',Btmp,'Statut__c',true,Context);
        Check(b,'ApvlReference__c',Btmp,'Reference__c',true,Context);
        Check(b,'ApvlVolumeAnnuelle__c',Btmp,'volumeAnnee__c',true,Context);
        Check(b,'ApvlCaAnnuel__c',Btmp,'caAnnuel__c',true,Context);
        Check(b,'ApvlMbAnnuel__c',Btmp,'mbAnnuel__c',true,Context);
        Check(b,'ApvlTauxCommission__c',Btmp,'TauxCommission__c',true,Context);
        Check(b,'ApvlprixPieceBrut__c',Btmp,'prixPieceBrut__c',true,Context);
        Check(b,'ApvlprixPieceNet__c',Btmp,'prixPieceNet__c',true,Context);
        Check(b,'ApvldescriptionPiece__c',Btmp,'descriptionPiece__c',true,Context);
        Check(b,'ApvlcaBrutAnnuel__c',Btmp,'caBrutAnnuel__c',true,Context);
        Check(b,'ApvlcaNetAnnuel__c',Btmp,'caNetAnnuel__c',true,Context);
    }
}