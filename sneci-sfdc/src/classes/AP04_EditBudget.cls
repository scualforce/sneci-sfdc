public class AP04_EditBudget {
    public static void checkPrerequisite(List<Opportunity> newList, List<Opportunity> oldList) {
        List<Opportunity> toEdit = new List<Opportunity>();                        
        //Verify that Opportunity's status went from Pending to Approved or Rejected. 
        for (Opportunity o : oldList) {
            if (o.stagename== Label.SuiviOpr && o.approvalStatusTech__c == Label.Opr_Pending) {
                String opId = o.Id;
                for (Opportunity n : newList) {
                    if ((n.Id == opId) && (n.stagename== Label.SuiviOpr) && ((n.approvalStatusTech__c == Label.Opr_Approved) || n.approvalStatusTech__c == Label.Opr_Rejected)) {
                        toEdit.add(n);
                    }
                }
            }
        }
        if (toEdit.size() > 0){
            Set<Id> OppId = new Set<Id>();
            for (Opportunity o : toEdit){
                OppId.add(o.Id);
            }
            EditHelper(OppId);
            system.debug('### oppScopeIds ='+ OppId);
        } 
    }
    
    public static void EditHelper(Set<Id> oppIds) {
        List<Budget__c> BdtToUp = new List<Budget__c>();
        List<Budget__c> BdtToDel = new List<Budget__c>();

        for(Budget__c b : [select Name, annee__c,mois__c,isApproval__c, opportunity__r.approvalStatusTech__c, Reference__c, volumeAnnee__c,tauxCommission__c,caAnnuel__c,mbAnnuel__c,prixPieceBrut__c,prixPieceNet__c ,caNetAnnuel__c,caBrutAnnuel__c,Statut__c,stateTec__c,ReferenceAPI__c,
                           ApvlAnnee__c,ApvlVolumeAnnuelle__c,ApvlReference__c, ApvlTauxCommission__c,ApvlCaAnnuel__c,ApvlMbAnnuel__c,ApvlPrixPieceBrut__c,ApvlPrixPieceNet__c,ApvlStatut__c, Opportunity__c,Opportunity__r.name,opportunity__r.numeroOpr__c, Opportunity__r.categorieService__c,ApvldescriptionPiece__c,descriptionPiece__c
                           FROM Budget__c where opportunity__c in:oppIds AND ISApproval__c = true ORDER BY annee__c asc]) { 
            
            if(b.opportunity__r.approvalStatusTech__c == Label.Opr_Approved) {
                if (b.stateTec__c == 'UP') {
                    b.caAnnuel__c        = b.ApvlCaAnnuel__c;
                    b.mbAnnuel__c        = b.ApvlMbAnnuel__c;
                    b.prixPieceBrut__c   = b.ApvlPrixPieceBrut__c;
                    b.prixPieceNet__c    = b.ApvlPrixPieceNet__c;
                    b.Reference__c       = b.ApvlReference__c;
                    b.descriptionPiece__c= b.ApvldescriptionPiece__c ;
                    b.tauxCommission__c  = b.ApvlTauxCommission__c;
                    b.volumeAnnee__c     = b.ApvlVolumeAnnuelle__c;
                    b.Statut__c          = b.ApvlStatut__c;
                    b.caBrutAnnuel__c	 = b.ApvlcaBrutAnnuel__c;
                    b.caNetAnnuel__c	 = b.ApvlcaNetAnnuel__c;
                    b.annee__c			 = b.apvlAnnee__c;
                    quotaBudget__mdt Taux = socleEditBudget.getTaux(b.Opportunity__r.categorieService__c);
                    socleEditBudget.CalcMensuel(b,(decimal)Taux.get(b.mois__c+'_mth__c'));
                    BdtToUp.add(b);
                } else if (b.stateTec__c == 'ADD') {
                    BdtToUp.add(b);
                } else if (b.stateTec__c == 'DEL') {
                    BdtToDel.add(b);
                }
            }
            else if (b.opportunity__r.approvalStatusTech__c == Label.Opr_Rejected) {
                if (b.stateTec__c == 'ADD') {
                    BdtToDel.add(b);
                } else {
                    BdtToUp.add(b);
                }
            }
            b.ApvlReference__c      = null;
            b.ApvlCaAnnuel__c       = null;
            b.ApvlMbAnnuel__c       = null;
            b.ApvlPrixPieceBrut__c  = null;
            b.ApvlPrixPieceNet__c   = null;
            b.ApvlAnnee__c 			= null;
            b.ApvlStatut__c         = null;
            b.ApvlTauxCommission__c = null;
            b.ApvlVolumeAnnuelle__c = null;
            b.ApvlcaBrutAnnuel__c 	= null;
            b.ApvlcaNetAnnuel__c 	= null;
            b.stateTec__c = null;
            b.isApproval__c = false;     
        }
        if (BdtToDel.size() > 0) {
            Database.deleteResult[] urs = Database.delete(BdtToDel,true);
        }
        if (BdtToUp.size() > 0) {
            Database.SaveResult[] urs = Database.update(BdtToUp,true);
        }   
    }
}