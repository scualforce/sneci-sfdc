public virtual  class socleEditBudget{
    protected id oprId = null;
    protected quotaBudget__mdt Taux = null;
    protected opportunity opr = null;
    public boolean isDisabled {set;get;}
    public boolean isOprApvlPending{get;set;}
    public boolean isUserOwnerOrAdmin{get;set;}
    public boolean isSuiviOpr{get;set;}
    public List<Budget__c> lstGlobalBdg{set;get;}
    public List<Budget__c> lstbgt {set;get;}
    public List<SelectOption> Annees {set;get;}
    public integer iStartlistAnnee {set;get;}
    public Map<String, Budget__c> mapOrigine = New Map<String, Budget__c>();
    public static list<string> lstMois = new list<string>{'JANVIER','FEVRIER','MARS','AVRIL','MAI','JUIN','JUILLET','AOUT','SEPTEMBRE','OCTOBRE','NOVEMBRE','DECEMBRE'};
    public integer sizeDetail {
        private set; 
        get{
            if(isOprApvlPending){
                integer i=0;
                for(Budget__c b:lstbgt){
                    i++;
                    if(b.stateTec__c=='UP'){
                        i++;
                    }
                }
                return i;  
            } else {
                return lstbgt.size();
            }  
        }
    }
    public socleEditBudget(ApexPages.StandardController controller) {
        isDisabled = true;
        lstbgt = new List<Budget__c>();
        lstGlobalBdg = new List<Budget__c>();
        opportunity oprTmp = (Opportunity) controller.getRecord();
        if(oprTmp != null){
            oprId = oprTmp.id;
            //init OPR
            refreshOpr();
            //get Taux
            Taux = getTaux(opr.categorieService__c);
            //init SelectOption Année
            iStartlistAnnee = opr.closeDate.year();
            majSelectAnnee();
            //init Budgets
            refreshBgt();
        }
    }
    protected void majSelectAnnee(){
        if(Annees==null){
            Annees = new list<SelectOption>();
        }
        Annees.clear();
        
        set<string> setAnnees = new set<string>();
        for(AggregateResult ar:[select Annee__c FROM Budget__c where opportunity__c =:opr.id group by Annee__c ORDER BY Annee__c asc]){
            setAnnees.add((string)ar.get('Annee__c'));
        }
        integer anneeToday = date.today().year();
        for(integer annee = iStartlistAnnee; annee<anneeToday+15;annee++){
            setAnnees.add(''+annee);
        }
        for(string annee:setAnnees){
            Annees.add(new SelectOption(''+annee,''+annee));
        }
        Annees.sort();

    }
    
    public static void CalcMensuel(budget__c bn,decimal quota) {
        if(bn.prixPieceBrut__c!=null && bn.volumeAnnee__c!=null){
            bn.caBrutAnnuel__c= bn.prixPieceBrut__c*bn.volumeAnnee__c;
        }
        if(bn.prixPieceNet__c!=null && bn.volumeAnnee__c!=null){
            bn.caNetAnnuel__c  = bn.prixPieceNet__c*bn.volumeAnnee__c;
        }
        if(bn.prixPieceNet__c!=null && bn.volumeAnnee__c!=null && bn.tauxCommission__c!=null){
            bn.caAnnuel__c = bn.tauxCommission__c*bn.prixPieceNet__c*bn.volumeAnnee__c/100;
            bn.mbAnnuel__c = bn.caAnnuel__c;
        }
        system.debug('##quota='+quota+' bn.caAnnuel__c='+bn.caAnnuel__c);
        bn.caMensuel__c = quota*bn.caAnnuel__c/100;
        bn.mbMensuel__c = quota*bn.MbAnnuel__c/100;
        
        if(bn.volumeAnnee__c!=null){
            bn.volumeMensuel__c = quota*bn.volumeAnnee__c/100;
        }
        
        if(bn.volumeMensuel__c !=null && bn.prixPieceBrut__c!=null){
            bn.caBrutmensuel__c = bn.volumeMensuel__c*bn.prixPieceBrut__c;
        }        
        
        bn.ExtID__c = ''+bn.annee__c+bn.mois__c+bn.opportunity__r.numeroOpr__c;
        if(bn.opportunity__r.numeroOpr__c.startsWith('BD')){
            bn.ExtID__c+=(''+bn.ReferenceAPI__c).replace('null','');
        } else {
            bn.ReferenceAPI__c=null;
        }
        bn.Name = ''+bn.annee__c+'-'+bn.mois__c+'-'+bn.opportunity__r.numeroOpr__c;//+('-'+bn.ReferenceAPI__c).replace('-null','');
    }
    //manager Taux
    private static Map<String, quotaBudget__mdt> mapTaux = initTaux();
    public static quotaBudget__mdt getTaux(string categorieService) {
        quotaBudget__mdt taux = mapTaux.get(categorieService);
        return (taux == null)?mapTaux.get('DEFAULT'):taux;
    }
    public static Map<String, quotaBudget__mdt> initTaux() {
        Map<String, quotaBudget__mdt> mapQuota = New Map<String, quotaBudget__mdt>();
        for (quotaBudget__mdt quota: [SELECT DeveloperName,JANVIER_mth__c, FEVRIER_mth__c,MARS_mth__c,AVRIL_mth__c,MAI_mth__c,JUIN_mth__c,JUILLET_mth__c ,AOUT_mth__c,OCTOBRE_mth__c,SEPTEMBRE_mth__c,NOVEMBRE_mth__c,DECEMBRE_mth__c FROM quotaBudget__mdt]) {
            mapQuota.put(quota.DeveloperName,quota);
        }
        return mapQuota;
    }    
    public string genExtId(Budget__c bO, string mois){
        return bO.annee__c+mois+bO.opportunity__r.numeroOpr__c+(''+bO.referenceAPI__c).replace('null','');
    }
    
    protected Void refreshOpr(){
        opr = [select id,Name,numeroOpr__c,stagename,approvalStatusTech__c ,CloseDate,Date_debut__c,categorieService__c,ownerID FROM opportunity where id=:oprId  limit 1];
        isOprApvlPending = (opr.approvalStatusTech__c == Label.Opr_Pending);
        isUserOwnerOrAdmin = (UserInfo.getUserId() == opr.ownerID || UserInfo.getProfileId() == Label.AdminProfileID);
        isSuiviOpr = (opr.stagename== Label.SuiviOpr);
    }
    
    public PageReference resetBgt() {
        refreshBgt();
        return null;
    }
    protected virtual void refreshBgt() {
        if(opr !=null){
            mapOrigine.clear();
            lstbgt.clear();
            isDisabled = true;
            for(Budget__c b:[select name,annee__c,mois__c,Statut__c,isApproval__c,ExtID__c,descriptionPiece__c,stateTec__c,opportunity__c,opportunity__r.numeroOpr__c,
                        volumeAnnee__c,Reference__c,ReferenceAPI__c,tauxCommission__c,caAnnuel__c,mbAnnuel__c,prixPieceBrut__c,prixPieceNet__c ,caNetAnnuel__c,caBrutAnnuel__c,ApvlStatut__c,ApvlAnnee__c,
                        ApvlVolumeAnnuelle__c,apvlReference__c,ApvlTauxCommission__c,ApvlCaAnnuel__c,ApvlMbAnnuel__c,ApvlPrixPieceBrut__c,ApvlPrixPieceNet__c,ApvldescriptionPiece__c,
                        ApvlcaBrutAnnuel__c ,ApvlcaNetAnnuel__c,(select id from Realises__r limit 1)
                        FROM Budget__c where opportunity__c =:opr.id AND mois__c='JANVIER' ORDER BY Reference__c,annee__c asc]){
                mapOrigine.put(b.ExtID__c,b.clone(false,false,false,false));
                b.name = b.name+math.random();
                lstbgt.add(b);
            }
            majSelectAnnee();
        }
    }
    public virtual PageReference AddOne() {
        isDisabled = false;
        string annee='';
        integer i = lstbgt.size();
        if(i>0){
            Budget__c bLast = lstbgt[i-1];
            Integer iAnnee = 0;
            if(bLast.annee__c!=null){
                iAnnee =Integer.Valueof(bLast.annee__c)+1;
            }
            lstbgt.add(new Budget__c(annee__c=''+iAnnee,Statut__c =bLast.Statut__c,reference__c =bLast.reference__c,descriptionPiece__c =bLast.descriptionPiece__c,stateTec__c = 'ADD',name=''+math.random()));
        } else {
            lstbgt.add(new Budget__c(annee__c=Annees[0].getValue(),stateTec__c = 'ADD',name=''+math.random()));
        }

        return null;
    }
    
    public PageReference DelOne(){
        isDisabled = false;
        string nameBudget = ApexPages.currentPage().getParameters().get('nameBudget');
        system.debug('##'+nameBudget);
        
        for(integer i=0;i<lstbgt.size();i++){
            budget__c b = lstbgt.get(i);
            if(b.Name == nameBudget){
                if(b.id!=null){
                    b.stateTec__c = 'DEL';
                } else{
                    lstbgt.remove(i);
                }
                break;
            }
            system.debug('##'+b.Name);
        }
        return null;
    }
    public PageReference UpOne(){
        isDisabled = false;
        string nameBudget = ApexPages.currentPage().getParameters().get('nameBudget');
        system.debug('##'+nameBudget );
        for(budget__c b:lstbgt){
            if(b.Name == nameBudget){
                system.debug('## found '+b.id );
                if(b.id!=null){
                    b.stateTec__c = 'UP';
                }
                break;
            }
        }
        calc();
        return null;
    }
    protected void calc() {
        for(Budget__c bgt:lstbgt ){
            if(bgt.prixPieceBrut__c!=null && bgt.volumeAnnee__c!=null){
                bgt.caBrutAnnuel__c= bgt.prixPieceBrut__c*bgt.volumeAnnee__c;
            }
            if(bgt.prixPieceNet__c!=null && bgt.volumeAnnee__c!=null){
                bgt.caNetAnnuel__c  = bgt.prixPieceNet__c*bgt.volumeAnnee__c;
            }
            if(bgt.prixPieceNet__c!=null && bgt.volumeAnnee__c!=null && bgt.tauxCommission__c!=null){
                bgt.caAnnuel__c = bgt.tauxCommission__c*bgt.prixPieceNet__c*bgt.volumeAnnee__c/100;
                bgt.mbAnnuel__c = bgt.caAnnuel__c;
            }     
        }
    }
    
    protected List<Budget__c> FlagToAdd(map<string,Budget__c> mapBudget){
        List<Budget__c> BdtToAdd = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            if(b.stateTec__c == 'ADD'){
                b.ReferenceAPI__c = (''+b.Reference__c+math.random()).replace('null','');
                for (String mois : lstMois) {
                    Budget__c bUp = b.clone(false,false,false,false);
                    if(bUp != null){
                        bUp.mois__c = mois;
                        bUp.Opportunity__r = opr;
                        bUp.Opportunity__c = oprId;
                        bUp.stateTec__c=null;
                        CalcMensuel(bUp,(decimal)Taux.get(mois+'_mth__c'));
                        BdtToAdd.add(bUp);
                    }
                }
            }
        }
        return BdtToAdd;
    }
    protected List<Budget__c> FlagTodelete(map<string,Budget__c> mapBudget){
        List<Budget__c> BdtToDel = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            if(b.stateTec__c == 'DEL'){
                Budget__c bO = mapOrigine.get(b.ExtID__c);
                for (String mois : lstMois) {
                    string extid = genExtId(bO,mois);
                    system.debug('### ExtId '+extid);  
                    Budget__c bDel = mapBudget.get(extid);
                    system.debug('### ExtId='+extid+'bDel='+bDel);  
                    if(bDel != null){
                        BdtToDel.add(bDel);
                    }
                }
            }
        }
        return BdtToDel;
    }
    protected List<Budget__c> FlagToUp(map<string,Budget__c> mapBudget){
        List<Budget__c> BdtToup = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            if(b.stateTec__c == 'UP'){
                Budget__c bO = mapOrigine.get(b.ExtID__c);
                string ReferenceAPI = (''+b.Reference__c+math.random()).replace('null','');
                for (String mois : lstMois) {
                    string extid = genExtId(bO,mois);
                    Budget__c bUp = b.clone(false,false,false,false);
                    bUp.Id = mapBudget.get(extid).id;
                    bUp.mois__c = mois;
                    bUp.ReferenceAPI__c = ReferenceAPI;
                    CalcMensuel(bUp,(decimal)Taux.get(mois+'_mth__c'));
                    bUp.stateTec__c=null;
                    BdtToUp.add(bUp);
                }
            }
        }
        return BdtToup;
    }
    
}