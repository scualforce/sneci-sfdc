@isTest(seealldata=false)
private Class VFC_OprProgressBar_test {
    public static User createClassicUser(string Username) {
        User u = new User(
            alias = 'classic',
            email=Username+'@testorg.com',
            emailencodingkey='UTF-8',
            lastname='Testing',
            languagelocalekey='fr',
            localesidkey='fr_FR',
            profileid = [SELECT Id FROM Profile WHERE Name = 'Standard Sneci' LIMIT 1][0].Id,
            timezonesidkey='America/Los_Angeles',
            username=Username+'@testorg.com'
        );
        insert u;
        return u;
    }    
    static testMethod void test() {
        user uRespBU = createClassicUser('RespBU');
        opportunity oprtmp = new opportunity(Name ='test',stagename= 'CREATION OPR',Bureau_SNECI_contractant__c = 'SNECI France',Responsable_BU__c = uRespBU.id);
        oprtmp.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ACT').getRecordTypeId();
        oprtmp.closedate = date.today();
        insert oprtmp ;
        opportunity opr= [SELECT id,RecordType.businessprocessid,histoEtape__c,stagename,approvalStatusTech__c FROM Opportunity where id =:oprtmp.id];
        PageReference pageRef = Page.VFP_OprProgressBar ;
        Test.setCurrentPageReference(pageRef);
        VFC_OprProgressBar vfc = new VFC_OprProgressBar(new ApexPages.StandardController(opr));
    }
    static testMethod void testREVISION() {
        user uRespBU = createClassicUser('RespBU');
        opportunity oprtmp = new opportunity(Name ='test',stagename= 'CREATION OPR',Bureau_SNECI_contractant__c = 'SNECI France',Responsable_BU__c = uRespBU.id);
        oprtmp.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ACT').getRecordTypeId();
        oprtmp.closedate = date.today();
        insert oprtmp ;
        oprtmp.stagename= 'REVISION OFFRE STANDARD';
        oprtmp.histoEtape__c='REDACTION OFFRE STANDARD';
        update oprtmp; 
        opportunity opr= [SELECT id,RecordType.businessprocessid,histoEtape__c,stagename,approvalStatusTech__c FROM Opportunity where id =:oprtmp.id];
        PageReference pageRef = Page.VFP_OprProgressBar ;
        Test.setCurrentPageReference(pageRef);
        VFC_OprProgressBar vfc = new VFC_OprProgressBar(new ApexPages.StandardController(opr));
    }
    static testMethod void testPending() {
        user uRespBU = createClassicUser('RespBU');
        opportunity oprtmp = new opportunity(Name ='test',stagename= 'CREATION OPR',approvalStatusTech__c='Pending',Bureau_SNECI_contractant__c = 'SNECI France',Responsable_BU__c = uRespBU.id);
        oprtmp.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ACT').getRecordTypeId();
        oprtmp.closedate = date.today();
        insert oprtmp ;
        opportunity opr= [SELECT id,RecordType.businessprocessid,histoEtape__c,stagename,approvalStatusTech__c FROM Opportunity where id =:oprtmp.id];
        PageReference pageRef = Page.VFP_OprProgressBar ;
        Test.setCurrentPageReference(pageRef);
        VFC_OprProgressBar vfc = new VFC_OprProgressBar(new ApexPages.StandardController(opr));
    }
}