public class VFC_OprProgressBar {
    public class State{
        public State(string label,string value,boolean active ){
            this.label = label;
            this.value = value;
            this.active = active;
            revision = false;
            approval = false;
        }
        public string label{get;set;}
        public string value {get;set;}
        public boolean active {get;set;}
        public boolean revision{get;set;}
        public boolean approval{get;set;}
        
    }
    public list<State> States {get;private set;}
    public string progress{get;private set;}
    public map<string,string> mapProsVente = new map<string,string>{'offre sur mesure'=>'SM','offre standard'=>'STD','offre BD projet'=>'BDP'};
    public VFC_OprProgressBar(ApexPages.StandardController controller) {
        // stock la traduction courant
        map<string,string> translateStage = new map<string,string>();
        for(Schema.PicklistEntry pl: Opportunity.stagename.getDescribe().getPicklistValues()){
            translateStage.put(pl.getValue(),pl.getLabel());
        }
        Opportunity opr = (Opportunity) controller.getRecord();
        system.debug('## opr:'+opr); 
        if(opr != null ) {
            BusinessProcess BP = [select name from businessprocess where id = :opr.RecordType.businessprocessid];
            string Alias = mapProsVente.get(BP.name)+'%';
            
            // obtient la liste de statut possible par processus de vente.
            List<EtapeOpr__mdt> lstetp=[select MasterLabel from EtapeOpr__mdt where DeveloperName Like :Alias and isShow__c=true ORDER BY DeveloperName];
            
            integer i=0;
            integer base = lstetp.size();
            if(base>0){
                States = new list<State>();
                base--;
                boolean isNotfound = true; 
                for(EtapeOpr__mdt e:lstetp){
                    States.add(new State(translateStage.get(e.MasterLabel),e.MasterLabel,isNotfound));
                    if(isNotfound && e.MasterLabel == opr.histoEtape__c){
                        isNotfound = false;
                    }
                    if(isNotfound){
                        i++;
                    }
                }
                boolean isNotfoundAp = true;

                for(State s:States) {
                    //le status est-il en Révision
                    if((opr.stagename == label.RevisionOffreStandand  && s.value == label.RedactionOffreStandard) 
                    || (opr.stagename == label.RevisionOffreSurMesure && s.value == label.RedactionOffreSurMesure) 
                    || (opr.stagename == label.RevisionRfqOffre       && s.value == label.RfqOffre) 
                    || (opr.stagename == label.RevisionContratSurMesure && s.value == label.RedactionContratSurMesure) 
                    || (opr.stagename == label.RevisionContratBdp     && s.value == label.RedactionOffreSurMesure)){
                        
                        s.revision = true;
                        s.active=false;
                        system.debug('### s.active='+s.revision);
                    }
                    //le status est-il en approbation
                    if(opr.approvalStatusTech__c == 'Pending' && (s.value == opr.stagename || s.revision)){
                        s.approval = true;
                        s.revision = false;
                        s.active = false;
                        system.debug('### s.active='+s.approval);
                    }
                }
            }
            // calcul la bar de progression
            progress=''+(i*100/base);
        } 
    }
}