public class AP01_oprApprovalManager {
    private static final string SURMESURE = '_sur_mesure';
    private static final string REDAC_OFFRE_STD = 'REDACTION OFFRE STANDARD';
    private static final string REDAC_OFFRE_SM = 'REDACTION OFFRE SUR MESURE';
    private static final string AP_CREATE_OPP = 'Approbation_Creation_opportunite';
    private static final string AP_CREATE_OPP_HBSF = 'Approbation_Creation_opportunite_HBSF';
    private static final string AP_CREATE_NBA = 'BDP_dde_valid_creation_NDA';
    private static final string AP_RFQ_OFFRE_BDP = 'BDP_dde_valid_RFQ_Offre';
    private static final string AP_RFQ_OFFRE_BDP_HBSF = 'BDP_dde_valid_RFQ_Offre_HBSF';
    private static final string AP_REDAC_OFFRE_SM = 'Offre_SM_dde_valid_redac_offre';
    private static final string AP_REDAC_OFFRE_STD = 'Offre_std_dde_valid_redac_offre';
    private static final string AP_REDAC_OFFRE_SM_HBSF = 'Offre_SM_dde_valid_redac_offre_HBSF';
    private static final string AP_REDAC_OFFRE_STD_HBSF = 'Offre_std_dde_valid_redac_offre_HBSF';
    private static final string AP_REDAC_CONTRAT_SM = 'Offre_SM_dde_valid_redac_contrat';
    private static final string AP_REDAC_CONTRAT_STD = 'Offre_std_dde_valid_redac_contrat';
    private static final string AP_REDAC_CONTRAT_BDP = 'BDP_dde_valid_redac_contrat';
    private static final string TEC_FIELD_REVISION = 'En cours de révision';
    private static final string APP_OPP_SOUS_CONTROL = AP_CREATE_OPP+AP_CREATE_OPP+AP_CREATE_NBA +AP_REDAC_CONTRAT_SM+AP_REDAC_CONTRAT_STD+AP_REDAC_CONTRAT_BDP+AP_REDAC_OFFRE_SM+AP_REDAC_OFFRE_STD+AP_RFQ_OFFRE_BDP+AP_RFQ_OFFRE_BDP_HBSF+AP_REDAC_OFFRE_SM_HBSF+AP_REDAC_OFFRE_STD_HBSF;
    private static final string APPS_REDAC_OFFRE = AP_REDAC_OFFRE_SM+AP_REDAC_OFFRE_STD+AP_REDAC_OFFRE_SM_HBSF+AP_RFQ_OFFRE_BDP_HBSF;
    private static final string APPS_OPP_REVISION = AP_REDAC_CONTRAT_SM+AP_REDAC_CONTRAT_STD+AP_REDAC_CONTRAT_BDP+AP_REDAC_OFFRE_SM+AP_REDAC_OFFRE_STD+AP_RFQ_OFFRE_BDP+AP_REDAC_OFFRE_SM_HBSF+AP_REDAC_OFFRE_STD_HBSF;
    public static Boolean canRun = true;
    private static map<string,string> mapReasonSNECI {
        get{
            if(mapReasonSNECI==null) {
                integer i=0;
                mapReasonSNECI = new map<string,string>();
                for(Schema.PicklistEntry pl: Opportunity.Cause_ABANDON_SNECI__c.getDescribe().getPicklistValues()){
                    i++;
                    mapReasonSNECI.put(''+i,pl.getLabel());
                }
            }
            return mapReasonSNECI;
        }
    }
    private static map<string,string> mapReasonSNECIValue {
        get{
            if(mapReasonSNECIValue ==null) {
                integer i=0;
                mapReasonSNECIValue = new map<string,string>();
                for(Schema.PicklistEntry pl: Opportunity.Cause_ABANDON_SNECI__c.getDescribe().getPicklistValues()){
                    i++;
                    mapReasonSNECI.put(''+i,pl.getValue());
                }
            }
            return mapReasonSNECI;
        }
    }
    public static void checkRejectedApproval(map<id,Opportunity> newMap,map<id,Opportunity> oldMap) {
        system.debug('##checkRejectedApproval Begin'); 
        map<id,Opportunity> lstOprScope = new map<id,Opportunity>();
        map<id,string> mapOprRt = new map<id,string>();
        List<Opportunity> oprToUp = new List<Opportunity>();
        set<id> oprIdsToLock = new set<id>();
        set<id> oprIdsSendEmail = new set<id>();
        //Mettre de le scope les Oprs qui sont Rejetées ou Approuvées
        for(Opportunity opr:newMap.values()){
            system.debug('## opr approvalStatusTech__c new:['+opr.approvalStatusTech__c+'] old:['+oldMap.get(opr.id).approvalStatusTech__c+']');
            if((opr.approvalStatusTech__c==Label.OPR_Rejected && oldMap.get(opr.id).approvalStatusTech__c==Label.Opr_Pending) ||
               (opr.approvalStatusTech__c==Label.Opr_Approved && oldMap.get(opr.id).approvalStatusTech__c==Label.Opr_Pending)){
                lstOprScope.put(opr.id,opr);
            }
        }
        system.debug('## SCOPE:'+lstOprScope);
        if(lstOprScope.size()>0) { //isNotEmpty()
            //stocker les RT des Oprs pour un traitement en mode bulk
            map<string,id> mapRtOprbyDevName = new map<string,id>();
            for (RecordType rt : [SELECT id,RecordType.DeveloperName FROM RecordType where SObjectType = 'Opportunity']){
                mapRtOprbyDevName.put(rt.DeveloperName,rt.id);
            }
            //stocker les APs en cours d'instance liée aux oprs
            List<Id> processInstanceIds = new List<Id>{};
            for (Opportunity opr : [SELECT id,RecordType.DeveloperName,(SELECT ID FROM ProcessInstances ORDER BY CreatedDate DESC LIMIT 1)
                                        FROM Opportunity WHERE ID IN :lstOprScope.keySet()]){
                system.debug('opr id = ' + opr.Id);
                if(opr.ProcessInstances.size()>0){
                    processInstanceIds.add(opr.ProcessInstances[0].Id);
                }
                mapOprRt.put(opr.id,opr.RecordType.DeveloperName);
            }
            //recherche les commentaires mis éditer par l'approbateur
            for (ProcessInstance pi : [SELECT TargetObjectId,ProcessDefinition.DeveloperName,
                                       (SELECT Id, StepStatus, Comments FROM Steps WHERE StepStatus in ('Approved','Rejected') ORDER BY CreatedDate DESC LIMIT 1 )
                                       FROM ProcessInstance WHERE Id IN :processInstanceIds ORDER BY CreatedDate DESC])
            {   
                id oprId = pi.TargetObjectId;
                string rtDevName = mapOprRt.get(oprId);
                string msg = null;
                string apStatus = lstOprScope.get(oprId).approvalStatusTech__c;
                string Comments = '';
                if(pi.Steps.size()>0){
                    Comments = pi.Steps[0].Comments;
                }
                System.debug('## pi='+pi.Steps);
                for (ProcessInstanceStep Step : pi.Steps){
                    System.debug('##Step='+Step);
                }
                string pdName = pi.ProcessDefinition.DeveloperName;
                final string ERRORCODE = 'le code "'+Comments+'" non comforme';
                System.debug('Opr=['+lstOprScope.get(oprId).name+'] apComment:['+Comments +'] apDeveloperName:['+pdName+']'+' apStatus=['+apStatus+']');
                // Effectuer le Traitement si le Commentaire est vide et que nous sommes en creation d'opr ou en rejet
                if((AP_CREATE_OPP+AP_CREATE_OPP_HBSF).contains(pdName) || (apStatus==Label.OPR_Rejected && APP_OPP_SOUS_CONTROL.contains(pdName))  || (string.isNotEmpty(Comments) && AP_CREATE_NBA.contains(pdName))){
                    if(string.isEmpty(Comments)){
                       // le Commentaire n'est été renseigné.
                       msg='= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = ='+
                           'Operation annulée, veuillez revenir en arrière et renseignez un "Code de '+((apStatus==Label.OPR_Rejected)?'rejet':'d\'approbations')+'" s\'il vous plait.';
                    } else {
                        // le Commentaire est été renseigné recherche du code.
                        Opportunity Opr = new Opportunity(id=oprId);
                        string Code = Comments.trim();
                        if(Comments.length()>1){
                            Code = Comments.substring(0,2).trim().toUpperCase();
                        }
                        
                        if(apStatus == Label.Opr_Approved) { //APPROVAL APPROVED
                            if((AP_CREATE_OPP+AP_CREATE_OPP_HBSF).contains(pdName) && Code=='A') { // APPROVED OFFRE STANDARD
                                Opr.StageName = Label.RedactionOffreStandard;
                                Opr.histoEtape__c = Label.RedactionOffreStandard;
                                Opr.Offre_sur_mesure__c = false;
                                if(rtDevName.contains(SURMESURE)){
                                    Opr.RecordTypeId = mapRtOprbyDevName.get(rtDevName.replace(SURMESURE,''));
                                }
                                oprToUp.add(Opr);                                 
                             } else if((AP_CREATE_OPP+AP_CREATE_OPP_HBSF).contains(pdName) && Code=='B') { // APPROVED OFFRE SUR MESURE
                                Opr.StageName = Label.RedactionOffreSurMesure;
                                Opr.histoEtape__c = Label.RedactionOffreSurMesure;
                                Opr.Offre_sur_mesure__c = true;
                                if(!rtDevName.contains(SURMESURE)){
                                    Opr.RecordTypeId = mapRtOprbyDevName.get(rtDevName+SURMESURE);
                                }
                                oprToUp.add(Opr);
                            } else if(pdName==AP_CREATE_NBA && Code=='A') { // APPROVED RFI
                                Opr.StageName = Label.Rfi;
                                Opr.histoEtape__c = Label.Rfi;
                                oprToUp.add(Opr);
                            } else {
                                msg = ERRORCODE; 
                            }
                        } else if(apStatus == Label.OPR_Rejected && Code.isnumeric()) {// APPROVAL REAL REJECTED  1,2 ou 3...
                            //approbation rejetée
                            Opr.Cause_ABANDON_SNECI__c = mapReasonSNECIValue.get(Code);
                            if(Opr.Cause_ABANDON_SNECI__c == null){
                                msg = ERRORCODE; 
                            } else{
                                Opr.StageName = Label.ArchivageRetex;
                                oprToUp.add(Opr);
                                oprIdsToLock.add(oprId);
                                oprIdsSendEmail.add(opr.id);
                            }
                        } else if(apStatus == Label.OPR_Rejected) { // APPROVAL REJECTED FOR REVISION
                            if(Code =='A') {
                            
                                if((AP_REDAC_OFFRE_SM+AP_REDAC_OFFRE_SM_HBSF).contains(pdName)){          //REVISION REDAC OFFFRE
                                    //Opr.StageName =Label.RevisionOffreSurMesure;
                                    Opr.validationOffre__c = TEC_FIELD_REVISION;
                                    Opr.Pieces_Redac_offre_jointes__c = false;
                                } else if((AP_REDAC_OFFRE_STD+AP_REDAC_OFFRE_STD_HBSF).contains(pdName)){ //REVISION REDAC OFFFRE
                                    //Opr.StageName =Label.RevisionOffreStandand;
                                    Opr.validationOffre__c = TEC_FIELD_REVISION;
                                    Opr.Pieces_Redac_offre_jointes__c = false;
                                } else if((AP_RFQ_OFFRE_BDP+AP_RFQ_OFFRE_BDP_HBSF).contains(pdName)){     //REVISION RFQ/OFFRE
                                    //Opr.StageName = Label.RevisionRfqOffre;
                                    Opr.validationOffre__c = TEC_FIELD_REVISION;
                                    Opr.Pieces_RFQ_OFFRE_jointes__c  = false;                                       
                                } else if(pdName == AP_REDAC_CONTRAT_BDP){                                //REVISION CONTRAT BDP
                                    //Opr.StageName = Label.RevisionContratBdp;
                                    Opr.validationContrat__c = TEC_FIELD_REVISION;
                                    Opr.Pieces_Redac_contrat_jointes__c = false;
                                } else if(pdName == AP_REDAC_CONTRAT_SM){                                 //REVISION CONTRAT SM
                                    //Opr.StageName = Label.RevisionContratSurMesure;
                                    Opr.validationContrat__c = TEC_FIELD_REVISION;
                                    Opr.Pieces_Redac_contrat_jointes__c = false;
                                } 
                                opr.approvalStatusTech__c = Label.Opr_Review;
                                oprToUp.add(Opr);
                                oprIdsSendEmail.add(opr.id);
                            } else {
                                msg = ERRORCODE; 
                            }
                        }
                    }
                    if(msg!=null){
                        lstOprScope.get(oprId).addError(msg,FALSE);
                    }
                    if(oprToUp.size()>0) {//isNotEmpty()
                        system.debug('## Up OPR:'+oprToUp);
                        update oprToUp;
                    }
                }
            }
            if(!oprIdsToLock.isEmpty()){
                AP03_LockManager.LockOpp(oprIdsToLock);
                system.debug('## LockResult ='+oprIdsToLock); 
            }   
            if(!oprIdsSendEmail.isEmpty()){
                AP02_EmailManager.sendMailRejetedOrReview(oprIdsSendEmail);
                system.debug('## IdsSendEmail='+oprIdsSendEmail); 
            }  
        }
        system.debug('##checkRejectedApproval End'); 
    }
   
    public static void SetInstruction(map<id,Opportunity> newMap,map<id,Opportunity> oldMap) {
        system.debug('## SetInstruction Begin'); 
        string msgInstruction='<b>'+Label.msgInstruction+' :</b>';
        string msgRevision ='<div style="margin-left: 40px;"><span style="color:orange">A</span>. '+Label.msgRevision+'<span style="color:orange">('+Label.msgRejeter+')</span></div>';
        string msgSurMesure ='<div style="margin-left: 40px;"><span style="color:green">A</span>.'+Label.msgStandard+'<span style="color:green">('+Label.msgApprouver+')</span></div>'+
                             '<div style="margin-left: 40px;"><span style="color:green">B</span>. '+Label.msgSurMesure+'<span style="color:green">('+Label.msgApprouver+')</span></div>';
        string msgRfi ='<div style="margin-left: 40px;"><span style="color:green">A</span>. RFI<span style="color:green">('+Label.msgApprouver+')</span></div>';
        string itemReason ='<span style="color:red">KEY</span>. VALUE<span style="color:red">('+Label.msgRejeter+')</span><br>';
        
        
        string msgReasonSNECI = '<div style="margin-left: 40px;">';
        for(string key:mapReasonSNECI.keySet()){
            msgReasonSNECI+=itemReason.replace('KEY',key).replace('VALUE',mapReasonSNECI.get(key));
        }
        msgReasonSNECI+='</div>';
        for(processInstance pi:[SELECT ProcessDefinition.DeveloperName,TargetObjectId,status FROM processInstance where status='Started' and TargetObjectId in:newMap.keyset()]){
            
            system.debug('## processInstance:'+pi.ProcessDefinition.DeveloperName+' pi:'+pi);
            if(''+pi.status=='Started'){
                Opportunity Opr = newMap.get(pi.TargetObjectId);
                string pdName = pi.ProcessDefinition.DeveloperName;
                system.debug('## processInstance:'+pi.ProcessDefinition.DeveloperName);
                string msgReason=null;
                string msgRevisiontmp='';
                msgReason = msgReasonSNECI;
                if((AP_CREATE_OPP+AP_CREATE_OPP_HBSF).contains(pdName)){
                    msgRevisiontmp=msgSurMesure;
                } else if(pdName == AP_CREATE_NBA){
                    msgRevisiontmp = msgRfi;
                } else if(APPS_OPP_REVISION.contains(pdName)){
                    msgRevisiontmp = msgRevision;
                }
                Opr.InstructionTech__c = msgInstruction+msgRevisiontmp+msgReason;
                system.debug('## pdName='+pdName+' Instruction:'+Opr.InstructionTech__c); 
            }
        }
        system.debug('## SetInstruction End'); 
    } 
}