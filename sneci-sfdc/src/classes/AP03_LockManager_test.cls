@isTest
private class AP03_LockManager_test {
    public static User createClassicUser(string Username) {
        User u = new User(
            alias = 'classic',
            email=Username+'@testorg.com',
            emailencodingkey='UTF-8',
            lastname='Testing',
            languagelocalekey='fr',
            localesidkey='fr_FR',
            profileid = [SELECT Id FROM Profile WHERE Name = 'Standard Sneci' LIMIT 1][0].Id,
            timezonesidkey='America/Los_Angeles',
            username=Username+'@testorg.com'
        );
        insert u;
        return u;
    }    
    @isTest static void test_LockOpp() {
        user uRespBU = createClassicUser('RespBU');
        test.startTest();
        Opportunity op = new Opportunity( name = 'loulou', CloseDate =  Date.Today(), StageName = 'CREATION OPR', Offre_sur_mesure__c = false,  validationOffre__c = 'Approuvée',Bureau_SNECI_contractant__c = 'SNECI France',Responsable_BU__c = uRespBU.id); 
        op.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ACT').getRecordTypeId();
        insert op;
        op.decisionClient__c = 'Révision';
        op.histoEtape__c = 'ARCHIVAGE/RETEX';
        op.StageName = 'ARCHIVAGE/RETEX';
        update op;
        Set<Id> ids = new set<Id>();
        ids.add(op.id);
        AP03_LockManager.LockOpp(ids);
        test.stopTest();
    }
    
}