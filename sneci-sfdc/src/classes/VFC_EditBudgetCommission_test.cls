@isTest
private class VFC_EditBudgetCommission_test {

    static VFC_EditBudgetCommission get_instance() {
        Opportunity op = new Opportunity(name = 'loulou', CloseDate =  Date.Today(),StageName = 'CREATION OPR', approvalStatusTech__c = Label.Opr_Pending,Bureau_SNECI_contractant__c = 'SNECI France', Responsable_BU__c = USerInfo.getUserId(), validationContrat__c = 'Approuvée');       
        op.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Business Developpement Projet').getRecordTypeId();
        insert op;
        VFC_EditBudgetCommission instce = new VFC_EditBudgetCommission(new ApexPages.StandardController(op));
        return instce;
    }
    
    @isTest static void test_AprlBgt() {
        Test.setCurrentPageReference(new PageReference('Page.VFP_EditBudgetCommission')); 
        VFC_EditBudgetCommission inst = get_instance();
        
        test.startTest();
        get_instance().setMode();
        integer i=inst.sizeGlobal;
        i=inst.sizeDetail;
        inst.AddOne();
        system.Assert(inst.lstbgt.size()==1,'AddOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.lstbgt[0].Reference__c = 'TBC1';
        inst.lstbgt[0].descriptionPiece__c = '12x45x78';
        inst.lstbgt[0].annee__c = '2019';
        inst.lstbgt[0].statut__c = 'ACQUIS';
        inst.lstbgt[0].volumeAnnee__c = 100000;
        inst.lstbgt[0].prixPieceBrut__c = 1600;
        inst.lstbgt[0].prixPieceNet__c = 1600;
        inst.lstbgt[0].tauxCommission__c = 2;
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[0].Name);
        inst.UpOne();
         
        inst.AddOne();
        system.Assert(inst.lstbgt.size()==2,'AddOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.lstbgt[1].Reference__c = 'TBC1';
        inst.lstbgt[1].descriptionPiece__c = '12x45x78';
        inst.lstbgt[1].annee__c = '2017';
        inst.lstbgt[1].statut__c = 'ACQUIS';
        inst.lstbgt[1].volumeAnnee__c = 100000;
        inst.lstbgt[1].prixPieceBrut__c = 1600;
        inst.lstbgt[1].prixPieceNet__c = 1600;
        inst.lstbgt[1].tauxCommission__c = 2;
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[1].Name);
        inst.UpOne();
        
        inst.AddOne();
        system.Assert(inst.lstbgt.size()==3,'AddOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.lstbgt[2].Reference__c = 'TBC1';
        inst.lstbgt[2].descriptionPiece__c = '12x45x78';
        inst.lstbgt[2].annee__c = '2016';
        inst.lstbgt[2].statut__c = 'ACQUIS';
        inst.lstbgt[2].volumeAnnee__c = 100000;
        inst.lstbgt[2].prixPieceBrut__c = 1600;
        inst.lstbgt[2].prixPieceNet__c = 1600;
        inst.lstbgt[2].tauxCommission__c = 2;
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[2].Name);
        inst.UpOne();
        inst.UpdateBgt();
        inst.resetBgt();
        
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[2].Name);
        inst.DelOne();
        
        inst.UpdateBgt();
        inst.resetBgt();
        
        inst.lstbgt[1].annee__c = '2020';
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[1].Name);
        inst.UpOne();
        
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[0].Name);
        inst.DelOne();
         
        inst.AddOne();
        system.Assert(inst.lstbgt.size()==3,'AddOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.lstbgt[2].Reference__c = 'TBC1';
        inst.lstbgt[2].descriptionPiece__c = '12x45x78';
        inst.lstbgt[2].annee__c = '2016';
        inst.lstbgt[2].statut__c = 'ACQUIS';
        inst.lstbgt[2].volumeAnnee__c = 100000;
        inst.lstbgt[2].prixPieceBrut__c = 1600;
        inst.lstbgt[2].prixPieceNet__c = 1600;
        inst.lstbgt[2].tauxCommission__c = 2;
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[2].Name);
        inst.UpOne();
        inst.AprlBgt();
        test.stopTest();
    }
 /*
    @isTest static void test_AprlBgt() {
        
        VFC_EditBudget inst = get_instance();
        bdgt.isApproval__c = false;
        bdgt.opportunity__c = inst.opr.Id;
        insert bdgt;
        inst.lstbgt.add(bdgt);
        inst.UpdateBgt();
        
        test.startTest();
        inst.AprlBgt();
        test.stopTest();
    }
    */
}