public class AP02_EmailManager {
    private static map<string,ID> MapEmailTpl = null;
    @future
    public static void sendMailRejetedOrReview(set<id> oppIds) {
        system.debug('## Begin sendMailRejetedOrReview:'+oppIds);
        if(MapEmailTpl == null){
           MapEmailTpl  = new map<string,ID> ();
           for(EmailTemplate et:[SELECT Id,DeveloperName  from EmailTemplate where DeveloperName LIKE 'VF_OPR_%']){
               MapEmailTpl.put(et.DeveloperName,et.id);
           }
        }
        map<id,list<string>> mapMailUserByOprId = new map<id,list<string>>();
        for(OpportunityTeamMember otm :[SELECT OpportunityId ,user.Email from OpportunityTeamMember where OpportunityId in:oppIds]){
            list<string> mails = mapMailUserByOprId.get(otm.OpportunityId);
            if(mails==null){
                mails = new list<string>();
                mapMailUserByOprId.put(otm.OpportunityId,mails);
            }
            mails.add(otm.user.Email);
        }
        
        List<Messaging.SingleEmailMessage> LstEmail = new List<Messaging.SingleEmailMessage>();
        for(opportunity opr:[SELECT id,ownerID,approvalStatusTech__c from opportunity where id in:oppIds]) {
            Id tplId = null;
            if(opr.approvalStatusTech__c == Label.Opr_Review){
               tplId = MapEmailTpl.get('VF_OPR_en_revision');
            } else if(opr.approvalStatusTech__c == Label.OPR_Rejected){
               tplId = MapEmailTpl.get('VF_OPR_Refus_definitif_de_validation');
            } else if(opr.approvalStatusTech__c == Label.Opr_Approved){
               tplId = MapEmailTpl.get('VF_OPR_Validation_etape');
            }
            if(tplId!=null){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setCcAddresses(mapMailUserByOprId.get(opr.id));       
                mail.setTargetObjectId(opr.ownerID);
                mail.setTemplateId(tplId);
                mail.setWhatId(opr.id);
                mail.setSaveAsActivity(false);
                LstEmail.add(mail);
            }
        }
        if(!LstEmail.isEmpty()){
            Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(LstEmail);
             system.debug('## resultMail ='+resultMail); 
        }
        system.debug('## Begin sendMailRejetedOrReview');        
    }
    @InvocableMethod
    public static void InvocableSendMailRejetedOrReview(List<ID> Ids) {
        system.debug('## Begin InvocablesendMailRejetedOrReview:'+Ids); 
        if(Ids.size()>0) {
            sendMailRejetedOrReview(new set<id>(Ids));
        }
        system.debug('## End InvocablesendMailRejetedOrReview'); 
    }
}