public class VFC_EditBudget {
    private id oprId;
    public opportunity opr {set;get;}
    public boolean isDetail {set;get;}
    public boolean isUpdateable {set;get;}
    public string OpportunityAccessLevel {set;get;}
    public List<Budget__c> lstBdgTete{set;get;}
    public List<Budget__c> lstbgt {set;get;}
    public boolean isDisabled {set;get;}
    public boolean isBusinessDeveloppement{set;get;}
    public boolean isOprApvlPending{get;set;}
    public boolean isUserRespBU{get;set;}
    public boolean isValidContrat{get;set;}
    public List<SelectOption> Annees {private set;get;}
    private Map<String, quotaBudget__mdt> mapQuota = New Map<String, quotaBudget__mdt>();
    private Map<String, Budget__c> mapOrigine = New Map<String, Budget__c>();
    public void refreshOpr(){
        opr = [select id,Name,numeroOpr__c,validationContrat__c,Responsable_BU__c,approvalStatusTech__c ,Date_debut__c,recordtype.developerName,categorieService__c FROM opportunity where id=:oprId  limit 1];
        isOprApvlPending = (opr.approvalStatusTech__c == Label.Opr_Pending);
        isUserRespBU = (UserInfo.getUserId() == opr.Responsable_BU__c);
        isValidContrat = (opr.validationContrat__c == 'Approuvée');
        isBusinessDeveloppement = opr.recordtype.developerName.contains('Business_Developpement');  // add Business_Developpement_Projet       
        //isBusinessDeveloppement = 'Business_Developpement;Business_Developpement_sur_mesure'.contains(opr.recordtype.developerName);            
    }
    
    private void refreshBgt() {
        if(opr !=null){
            mapOrigine.clear();
            lstbgt.clear();
            lstBdgTete.clear();
            for(Budget__c b:[select annee__c,mois__c,isApproval__c,ExtID__c,descriptionPiece__c,
                        volumeAnnee__c,Reference__c,tauxCommission__c,caAnnuel__c,mbAnnuel__c,prixPieceBrut__c,prixPieceNet__c ,caNetAnnuel__c,caBrutAnnuel__c,Statut__c,opportunity__r.numeroOpr__c,
                        ApvlVolumeAnnuelle__c,apvlReference__c,ApvlTauxCommission__c,ApvlCaAnnuel__c,ApvlMbAnnuel__c,ApvlPrixPieceBrut__c,ApvlPrixPieceNet__c,ApvlStatut__c FROM Budget__c where opportunity__c =:opr.id AND mois__c='JANVIER' ORDER BY Reference__c,annee__c asc]){
                mapOrigine.put(b.ExtID__c,b.clone(false,false,false,false)); 
                lstbgt.add(b);
                if(b.isApproval__c){
                    Budget__c bApp = b.clone(false,false,false,false);
                    b.isApproval__c = false;
                    bApp.Statut__c = bApp.ApvlStatut__c ;
                    bApp.Reference__c = bApp.apvlReference__c;
                    bApp.volumeAnnee__c = bApp.ApvlVolumeAnnuelle__c;
                    bApp.prixPieceBrut__c = bApp.ApvlPrixPieceBrut__c;
                    bApp.prixPieceNet__c = bApp.ApvlPrixPieceNet__c;
                    bApp.tauxCommission__c = bApp.ApvlTauxCommission__c;
                    bApp.caAnnuel__c = bApp.ApvlCaAnnuel__c;
                    bApp.mbAnnuel__c = bApp.ApvlMbAnnuel__c;
                    lstbgt.add(bApp);
                }
                if(!isBusinessDeveloppement ){
                    for(SelectOption so:Annees){
                        if(b.annee__c==so.getValue()){
                            so.setDisabled(true);
                        }
                    }
                }
            }
            calc();
            for(AggregateResult ar:[select Annee__c,Statut__c,SUM(caAnnuel__c) sumCA,SUM(caBrutAnnuel__c) sumCABrut,SUM(caNetAnnuel__c) sumCANet
                                    FROM Budget__c where opportunity__c =:opr.id
                                    AND mois__c ='JANVIER' 
                                    group by Annee__c,Statut__c ORDER BY Annee__c]){
                 Budget__c bt = new Budget__c ();
                 bt.Annee__c = (string)ar.get('Annee__c');
                 bt.Statut__c = (string)ar.get('Statut__c');
                 bt.caAnnuel__c = (decimal)ar.get('sumCA');
                 bt.caBrutAnnuel__c = (decimal)ar.get('sumCABrut');
                 bt.caNetAnnuel__c = (decimal)ar.get('sumCANet');
                 for(Budget__c b:lstbgt){
                     if(bt.Annee__c == b.Annee__c && b.isApproval__c){
                         bt.isApproval__c = true;
                         break;
                     }
                 }
                 lstBdgTete.add(bt);
            }    
        }
    }   

    public VFC_EditBudget(ApexPages.StandardController controller) {
        

        isDetail=false;
        isDisabled = true;
        lstbgt = new List<Budget__c>();
        lstBdgTete = new List<Budget__c>();
        opportunity oprTmp = (Opportunity) controller.getRecord();
        if(oprTmp != null){
            oprId = oprTmp.id;
            id userId = userinfo.getUserId();
            isUpdateable = false;
            OpportunityAccessLevel = 'none';
            for(opportunityShare oppShare:[select OpportunityAccessLevel FROM opportunityShare where UserOrGroupId=:userId and OpportunityId=:oprId]){
                
                OpportunityAccessLevel = oppShare.OpportunityAccessLevel;
                if(oppShare.OpportunityAccessLevel.contains('All') || oppShare.OpportunityAccessLevel.contains('Edit')){
                    isUpdateable = true;
                }
            }
            //init OPR
            refreshOpr();
            //init Quota
            for (quotaBudget__mdt quota: [SELECT DeveloperName,JANVIER_mth__c, FEVRIER_mth__c,MARS_mth__c,AVRIL_mth__c,MAI_mth__c,JUIN_mth__c,JUILLET_mth__c ,AOUT_mth__c,OCTOBRE_mth__c,SEPTEMBRE_mth__c,NOVEMBRE_mth__c,DECEMBRE_mth__c FROM quotaBudget__mdt]) {
                mapQuota.put(quota.DeveloperName,quota);
            }
            refreshOpr();
            //init SelectOption Année
            integer anneeToday = date.today().year();
            integer anneeRef = (opr.Date_debut__c!=null)?opr.Date_debut__c.year():anneeToday;
            Annees = new list<SelectOption>();
            for(integer annee = anneeRef; annee<anneeToday+5;annee++){
                Annees.add(new SelectOption(''+annee,''+annee));
            }
            //init Budgets
            refreshBgt();
        }
    }
    private string GetMd5(Budget__c b){
         return EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(''+b.getPopulatedFieldsAsMap())));
    }
    
    public PageReference AddOne() {
        string annee='';
        integer i = lstbgt.size();
        /*if(i<Annees.size()){
            annee = Annees[i].getValue();
        }*/
        if(i>0){
            Budget__c bLast = lstbgt[i-1];
            Integer iAnnee = 0;
            if(bLast.annee__c!=null){
                iAnnee =Integer.Valueof(bLast.annee__c)+1;
            }
            lstbgt.add(new Budget__c(annee__c=''+iAnnee,Statut__c =bLast.Statut__c,Reference__c= bLast.Reference__c));
        } else {
            lstbgt.add(new Budget__c(annee__c=Annees[0].getValue()));
        }    
        return null;
    }
    public PageReference upEdit() {
        isDisabled = false;
        return null;
    }
    public PageReference upEditbd() {
        isDisabled = false;
        calc();
        return null;
    }
    private void calc() {
        for(Budget__c bgt:lstbgt ){
            if(bgt.prixPieceBrut__c!=null && bgt.volumeAnnee__c!=null){
                bgt.caBrutAnnuel__c= bgt.prixPieceBrut__c*bgt.volumeAnnee__c;
            }
            if(bgt.prixPieceNet__c!=null && bgt.volumeAnnee__c!=null){
                bgt.caNetAnnuel__c  = bgt.prixPieceNet__c*bgt.volumeAnnee__c;
            }
            if(bgt.prixPieceNet__c!=null && bgt.volumeAnnee__c!=null && bgt.tauxCommission__c!=null){
                bgt.caAnnuel__c = bgt.tauxCommission__c*bgt.prixPieceNet__c*bgt.volumeAnnee__c/100;
                bgt.mbAnnuel__c = bgt.caAnnuel__c;
            }     
        }
    }
    public PageReference UpdateBgt() {
        isDisabled = true;
        List<Budget__c> BdtToUp = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            quotaBudget__mdt quotas = mapQuota.get(opr.categorieService__c);
            if(quotas == null){
               quotas = mapQuota.get('DEFAULT');
            }
            if(b.opportunity__c==null){
                b.opportunity__c = opr.id;
            }
            Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.quotaBudget__mdt.fields.getMap();
            for (String fieldName : schemaFieldMap.keySet()) {
                if(fieldName.endswith('_mth__c')){
                    Budget__c bn = b.clone(false,false,false,false);
                    BdtToUp.add(bn);
                    decimal quota = (decimal)quotas.get(fieldName);
                    bn.mois__c = fieldName.replace('_mth__c','');
                    if(bn.prixPieceBrut__c!=null && bn.volumeAnnee__c!=null){
                        bn.caBrutAnnuel__c= bn.prixPieceBrut__c*bn.volumeAnnee__c;
                    }
                    if(bn.prixPieceNet__c!=null && bn.volumeAnnee__c!=null){
                        bn.caNetAnnuel__c  = bn.prixPieceNet__c*bn.volumeAnnee__c;
                    }
                    if(bn.prixPieceNet__c!=null && bn.volumeAnnee__c!=null && bn.tauxCommission__c!=null){
                        bn.caAnnuel__c = bn.tauxCommission__c*bn.prixPieceNet__c*bn.volumeAnnee__c/100;
                        bn.mbAnnuel__c = bn.caAnnuel__c;
                    }  
                    bn.caMensuel__c = quota*bn.caAnnuel__c/100;
                    bn.mbMensuel__c = quota*bn.MbAnnuel__c/100;
                    if(bn.volumeAnnee__c!=null){
                        bn.volumeMensuel__c = quota*bn.volumeAnnee__c/100;
                    }                        
                    bn.ExtID__c = ''+bn.annee__c+bn.mois__c+opr.numeroOpr__c+(''+bn.Reference__c).replace('null','');
                    bn.Name = ''+bn.annee__c+' '+bn.mois__c;
                }
            }
        }
        if(BdtToUp.size()>0){
            Database.UpsertResult[] urs = Database.upsert(BdtToUp , Budget__c.Fields.ExtID__c, false);
            for(Database.UpsertResult ur:urs){
                if(!ur.isSuccess()) {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,''+ur.getErrors()));
                }
            }
            refreshBgt();
        }
        return null;
    }
    private boolean isDiff(Budget__c b1,Budget__c b2){
        string strb1 = b1.statut__c+(isBusinessDeveloppement?(''+b1.Reference__c+b1.volumeAnnee__c+b1.prixPieceBrut__c+b1.prixPieceNet__c+b1.TauxCommission__c):(''+b1.CaAnnuel__c+b1.MbAnnuel__c));
        string strb2 = b2.statut__c+(isBusinessDeveloppement?(''+b2.Reference__c+b2.volumeAnnee__c+b2.prixPieceBrut__c+b2.prixPieceNet__c+b2.TauxCommission__c):(''+b2.CaAnnuel__c+b2.MbAnnuel__c));
        system.debug('### '+strb1);
        system.debug('### '+strb2);
        return(strb1!=strb2);
    }
    
    public PageReference AprlBgt() {
        isDisabled = true;
        boolean isSubmitAppRequest = true;
        List<Budget__c> BdtToUp = new List<Budget__c>();
        for(Budget__c b:lstbgt) {
            Budget__c bUp = mapOrigine.get(b.ExtId__c);
            if(isDiff(b,bUp) && b.isApproval__c==false){
                bUp.isApproval__c = true;
                bUp.ApvlStatut__c = b.statut__c ;
                bUp.ApvlVolumeAnnuelle__c= b.volumeAnnee__c ;
                bUp.apvlReference__c = b.Reference__c;
                bUp.ApvlprixPieceBrut__c= b.prixPieceBrut__c;
                bUp.ApvlprixPieceNet__c  = b.prixPieceNet__c;
                bUp.ApvltauxCommission__c= b.TauxCommission__c;
                bUp.ApvlcaAnnuel__c= b.CaAnnuel__c;
                bUp.ApvlmbAnnuel__c= b.MbAnnuel__c;
                BdtToUp.add(bUp);
            }
        }
        if(BdtToUp.size()>0){
            Database.UpsertResult[] urs = Database.upsert(BdtToUp , Budget__c.Fields.ExtID__c, false);
            for(Database.UpsertResult ur:urs){
                if(!ur.isSuccess()) {
                    isSubmitAppRequest = false;
                    isDisabled = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,''+ur.getErrors()));
                }
            }
            refreshBgt();
        }
        if(isSubmitAppRequest ){
            Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
            req1.setObjectId(oprid);
            req1.setComments('Merci de valider la modification des budgets.');
            req1.setProcessDefinitionNameOrId('ApvlBudget');
            req1.setSkipEntryCriteria(true);
            Approval.ProcessResult result = Approval.process(req1);
            refreshOpr();
        }
        return null;
    }
    public PageReference setMode() {
        isDetail=!isDetail;
        return null;
    }
}