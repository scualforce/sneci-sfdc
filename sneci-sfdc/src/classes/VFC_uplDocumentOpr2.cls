public class VFC_uplDocumentOpr2 {
    public map<string,string> mapProsVente = new map<string,string>{'offre sur mesure'=>'SM','offre standard'=>'STD','offre BD projet'=>'BDP'};
    public blob bFile{get;set;}  // binaire fichier upload
    public string msg{get;set;}
    public string FileName{get;set;}
    public EtapeOpr__mdt etapeOpp{get;set;}
    public boolean showUpFile {get;set;}
    public string type = null;
    private Opportunity opr = null;
    public map<string,attachment> atts  {get;set;}
    public VFC_uplDocumentOpr2(ApexPages.StandardController controller) {
        atts = new map<string,attachment>{'input'=>new attachment(),'nda'=>new attachment(),'rfi'=>new attachment(),'rfq'=>new attachment(),'offre'=>new attachment(),'contrat'=>new attachment(),'signature'=>new attachment()};
        opr = (Opportunity) controller.getRecord();
        BusinessProcess BP = [select name from businessprocess where id = :opr.RecordType.businessprocessid];
        string Alias = mapProsVente.get(BP.name)+'%';
        system.debug('### Alias ='+Alias +' BP.name='+BP.name+' opr.stagename='+opr.stagename); 
        List<EtapeOpr__mdt> EtapeOprs = [select MasterLabel,DeveloperName ,pjField__c ,msgAp__c,msgES__c,typeFichier__c,isShow__c from EtapeOpr__mdt where MasterLabel=:opr.stagename AND DeveloperName Like :Alias];
        system.debug('### EtapeOprs='+EtapeOprs); 
        if(!EtapeOprs.isEmpty()){
            etapeOpp = EtapeOprs[0];
             system.debug('### etapeOpp ='+etapeOpp ); 
            msg = (opr.approvalStatusTech__c=='Pending')?etapeOpp.msgAp__c:etapeOpp.msgES__c;
            showUpFile = false;
            if(string.IsNotEmpty(etapeOpp.pjField__c) && opr.get(etapeOpp.pjField__c)!=null){    
                showUpFile = !(boolean)opr.get(etapeOpp.pjField__c);
            }
            
            for(attachment a:[select id,name from attachment where parentid =:opr.id order by createddate]){
                atts.put(a.name.split('\\.',-1)[0],a);
            }
        }
    }
    public PageReference Upload() {
        msg ='';
        if(string.IsNotEmpty(etapeOpp.pjField__c) && opr.get(etapeOpp.pjField__c)!=null){    
            opr.put(etapeOpp.pjField__c,true);
            showUpFile = false;
            msg = etapeOpp.msgAp__c;
        }
        integer version = 1;
        List<string> lst = ((string)(''+atts.get(etapeOpp.typeFichier__c).name)).replace('.v','.').split('\\.',-1);
        if(lst.size()>2 && lst[1].isNumeric()){
            version = integer.valueOf(lst[1])+1;            
        }
        Attachment att = new Attachment(  
            ParentId = opr.id,
            Name = etapeOpp.typeFichier__c+'.v'+('0'+version).right(2)+'.'+FileName,
            Description = '',
            Body = bFile
        );
        try{
            system.debug('## begin insert att'); 
            insert att;
            bFile=null;
            att.Body=null;
            atts.put(etapeOpp.typeFichier__c,att);
            system.debug('## end insert att');
            system.debug('## begin update opr'); 
            update opr;
            system.debug('## end update opr');
        } catch(exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,e.getmessage()));
        }
        return null;
    }
}