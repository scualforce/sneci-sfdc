@isTest(seealldata=false)
private Class VFC_uplDocumentOpr2_test{
    public static User createClassicUser(string Username) {
        User u = new User(
            alias = 'classic',
            email=Username+'@testorg.com',
            emailencodingkey='UTF-8',
            lastname='Testing',
            languagelocalekey='fr',
            localesidkey='fr_FR',
            profileid = [SELECT Id FROM Profile WHERE Name = 'Standard Sneci' LIMIT 1][0].Id,
            timezonesidkey='America/Los_Angeles',
            username=Username+'@testorg.com'
        );
        insert u;
        return u;
    }    
    static testMethod void test() {
         user uRespBU = createClassicUser('RespBU');
        opportunity oprtmp = new opportunity(Name ='test',stagename= 'CREATION OPR',Bureau_SNECI_contractant__c = 'SNECI France',Responsable_BU__c = uRespBU.id);
        oprtmp.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ACT').getRecordTypeId();
        oprtmp.closedate = date.today();
        insert oprtmp ;
        attachment att = new attachment(name='input.v00.toto.txt',body=Blob.valueof('test'),parentID=oprtmp .id);
        insert att;
        opportunity opr= [SELECT id,RecordType.businessprocessid,histoEtape__c,stagename,approvalStatusTech__c,Pieces_Input_jointes__c FROM Opportunity where id =:oprtmp.id];
        test.startTest();
        PageReference pageRef = Page.VFP_uplDocumentOpr2;
        Test.setCurrentPageReference(pageRef);
        VFC_uplDocumentOpr2 vfc = new VFC_uplDocumentOpr2(new ApexPages.StandardController(opr));
        vfc.bFile = Blob.valueof('test');
        vfc.Upload();
        test.stopTest();
    }
    static testMethod void testFail() {
         user uRespBU = createClassicUser('RespBU');
        opportunity oprtmp = new opportunity(Name ='test',stagename= 'CREATION OPR',Bureau_SNECI_contractant__c = 'SNECI France',Responsable_BU__c = uRespBU.id);
        oprtmp.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ACT').getRecordTypeId();
        oprtmp.closedate = date.today();
        insert oprtmp ;
        attachment att = new attachment(name='input.v00.toto.txt',body=Blob.valueof('test'),parentID=oprtmp .id);
        insert att;
        opportunity opr= [SELECT id,RecordType.businessprocessid,histoEtape__c,stagename,approvalStatusTech__c,Pieces_Input_jointes__c FROM Opportunity where id =:oprtmp.id];
        test.startTest();
        PageReference pageRef = Page.VFP_uplDocumentOpr2;
        Test.setCurrentPageReference(pageRef);
        VFC_uplDocumentOpr2 vfc = new VFC_uplDocumentOpr2(new ApexPages.StandardController(opr));
        //vfc.bFile = Blob.valueof('test');
        vfc.Upload();
        test.stopTest();
    }
}