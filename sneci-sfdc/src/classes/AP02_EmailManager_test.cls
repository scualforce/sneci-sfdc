@isTest
private class AP02_EmailManager_test {
    @isTest static void test_InvocableSendMailRejetedOrReview() {
        Opportunity op = new Opportunity(name = 'loulou', CloseDate =  Date.Today(),StageName = 'CREATION OPR', Responsable_BU__c = USerInfo.getUserId(), validationContrat__c = 'Approuvée', approvalStatusTech__c = Label.Opr_Review,Bureau_SNECI_contractant__c = 'SNECI France');
        Opportunity op1 = new Opportunity(name = 'lala', CloseDate =  Date.Today(),StageName = 'CREATION OPR', Responsable_BU__c = USerInfo.getUserId(), validationContrat__c = 'Approuvée', approvalStatusTech__c = Label.Opr_Approved,Bureau_SNECI_contractant__c = 'SNECI France');
        Opportunity op2 = new Opportunity(name = 'fifi', CloseDate =  Date.Today(),StageName = 'CREATION OPR', Responsable_BU__c = USerInfo.getUserId(), validationContrat__c = 'Approuvée', approvalStatusTech__c = Label.Opr_Rejected,Bureau_SNECI_contractant__c = 'SNECI France');               
        op.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Business Developpement Projet').getRecordTypeId();
        insert op;
        insert op1;
        insert op2;

        List<Id> opi = new List<Id>();
        opi.add(op.id);
        opi.add(op1.id);
        opi.add(op2.id);

        test.startTest();
        AP02_EmailManager.InvocableSendMailRejetedOrReview(opi);
        test.stopTest();
    }
    
    
}