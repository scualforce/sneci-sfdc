@isTest
private class VFC_EditBudget_test {
    
    private static Budget__c bdgt = new Budget__c(statut__c = 'POTENTIEL', Reference__c = 'test', CaAnnuel__c = 12, MbAnnuel__c = 87, ExtId__c = 'yolo',mois__c = 'JANVIER', isApproval__c = true, annee__c = '2016', prixPieceNet__c = 12, tauxCommission__c = 3, volumeAnnee__c = 45, prixPieceBrut__c = 78);

    @isTest static VFC_EditBudget get_instance() {
        Opportunity op = new Opportunity(name = 'loulou', CloseDate =  Date.Today(),StageName = 'CREATION OPR', approvalStatusTech__c = Label.Opr_Pending,Bureau_SNECI_contractant__c = 'SNECI France', Responsable_BU__c = USerInfo.getUserId(), validationContrat__c = 'Approuvée');       
        op.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Business Developpement Projet').getRecordTypeId();
        insert op;
        ApexPages.StandardController c = new ApexPages.StandardController(op);
        VFC_EditBudget instce = new VFC_EditBudget(c);
        return instce;
    }

    @isTest static void test_refreshOpr() {
        
        test.startTest();  
        get_instance().refreshOpr();
        test.stopTest();
    }
    
    @isTest static void test_AddOne() {
        
        VFC_EditBudget inst = get_instance();
        bdgt.opportunity__c = inst.opr.Id; 
        Budget__c bdt = bdgt.clone(false, true, false, false);

        test.startTest();
        inst.AddOne();
        inst.lstbgt.add(bdgt);
        inst.lstbgt.add(bdt);
        inst.AddOne();
        test.stopTest();
    }
    
    @isTest static void test_upEdit() {
        
        VFC_EditBudget inst = get_instance();
        bdgt.opportunity__c = inst.opr.Id; 
        Budget__c bdt = new Budget__c();
        inst.lstbgt.add(bdgt);
        inst.lstbgt.add(bdt);

        test.startTest();
        inst.upEdit();
        test.stopTest();
    }

    @isTest static void test_upEditbd() {
        
        VFC_EditBudget inst = get_instance();
        Budget__c bdt = new Budget__c();
        inst.lstbgt.add(bdgt);
        inst.lstbgt.add(bdt);

        test.startTest();
        inst.upEditbd();
        test.stopTest();
    }

    @isTest static void test_UpdateBgt() {
        
        Budget__c bdt = new Budget__c();
        VFC_EditBudget inst = get_instance();
        inst.lstbgt.add(bdgt);

        test.startTest();
        inst.UpdateBgt();
        test.stopTest();
    }

    @isTest static void test_setMode() {
        
        test.startTest();  
        get_instance().setMode();
        test.stopTest();
    }

    @isTest static void test_AprlBgt() {

        VFC_EditBudget inst = get_instance();
        bdgt.isApproval__c = false;
        bdgt.opportunity__c = inst.opr.Id;
        insert bdgt;
        inst.lstbgt.add(bdgt);
        inst.UpdateBgt();

        test.startTest();
        inst.AprlBgt();
        test.stopTest();
    }

}