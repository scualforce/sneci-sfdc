public class AP03_LockManager {
    @future
    public static void LockOpp(set<id> oppIds) {
        system.debug('## Begin LockOpp:'+oppIds); 
        Opportunity[] opps = [select id,StageName from opportunity where id in: oppIds ];
        Approval.lockResult[] lrs= Approval.lock(opps, false);
        for(Approval.LockResult lr : lrs) {
            System.debug('##locked Opr:' + lr.getId()+' isOk:'+lr.isSuccess());
        }
        system.debug('## End LockOpp');
    }
    @InvocableMethod
    public static void InvocableLockOppFromPB(List<ID> Ids) {
        system.debug('## Begin InvocableLockOppFromPB:'+Ids);
        Opportunity op = [select Stagename from Opportunity WHERE id in :Ids];
        map<id,Opportunity> LockOpp = new map<id,Opportunity>([SELECT Id FROM Opportunity WHERE id in :Ids AND Stagename='ARCHIVAGE/RETEX']);
        if(LockOpp.size()>0) {
            LockOpp(LockOpp.keyset());
        }
        system.debug('## End InvocableLockOppFromPB');
    }
}