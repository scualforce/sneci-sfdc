@isTest
private class VFC_EditBudgetPrestation_test {
    
    @isTest static void test_AprlBgt() {
        Opportunity op = new Opportunity(name = 'loulou', CloseDate =  Date.Today(),StageName = 'CREATION OPR', approvalStatusTech__c = null,Bureau_SNECI_contractant__c = 'SNECI France', Responsable_BU__c = USerInfo.getUserId(), validationContrat__c = 'Approuvée');       
        op.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ACT').getRecordTypeId();
        insert op;
        Test.setCurrentPageReference(new PageReference('Page.VFP_EditBudgetCommission')); 
        VFC_EditBudgetPrestation inst = new VFC_EditBudgetPrestation(new ApexPages.StandardController(op));
                
        test.startTest();
        inst.AddOne();
        integer sizebgt = inst.sizeDetail;
        system.Assert(inst.lstbgt.size()==1,'AddOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.lstbgt[0].caAnnuel__c = 200145;
        inst.lstbgt[0].mbAnnuel__c = 200145;
        inst.lstbgt[0].annee__c = '2016';
        inst.lstbgt[0].statut__c = 'ACQUIS';
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[0].Name);
        inst.UpOne();
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[0].Name);
        inst.DelOne();
        
        inst.AddOne();
        system.Assert(inst.lstbgt.size()==1,'AddOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.lstbgt[0].caAnnuel__c = 200145;
        inst.lstbgt[0].mbAnnuel__c = 200145;
        inst.lstbgt[0].annee__c = '2016';
        inst.lstbgt[0].statut__c = 'ACQUIS';
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[0].Name);
        inst.UpOne();
         
        inst.AddOne();
        system.Assert(inst.lstbgt.size()==2,'AddOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.lstbgt[1].caAnnuel__c = 200145;
        inst.lstbgt[1].mbAnnuel__c = 200145;
        inst.lstbgt[1].annee__c = '2017';
        inst.lstbgt[1].statut__c = 'ACQUIS';
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[1].Name);
        inst.UpOne();
        
        inst.AddOne();
        system.Assert(inst.lstbgt.size()==3,'AddOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.lstbgt[2].caAnnuel__c = 200145;
        inst.lstbgt[2].mbAnnuel__c = 200145;
        inst.lstbgt[2].annee__c = '2019';
        inst.lstbgt[2].statut__c = 'ACQUIS';
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[2].Name);
        inst.UpOne();
        inst.UpdateBgt();
        inst.resetBgt();

        inst.lstbgt[1].statut__c = 'ACQUIS';
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[1].Name);
        inst.UpOne();
        
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[0].Name);
        //system.Assert(System.currentPageReference().getParameters().get('nameBudget')==inst.lstbgt[0].Name,'nameBudget='+System.currentPageReference().getParameters().get('nameBudget')+' inst.lstbgt[0].Name='+inst.lstbgt[0].Name);
        inst.DelOne();
        //system.Assert(inst.lstbgt.size()==2,'DelOne fail lstbgt.size()='+inst.lstbgt.size());
        inst.UpdateBgt();
        inst.resetBgt();
        

        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[0].Name);
        inst.DelOne();
         
        System.currentPageReference().getParameters().put('nameBudget', inst.lstbgt[1].Name);
        inst.UpOne();
        inst.AprlBgt();
        sizebgt = inst.sizeDetail;
        test.stopTest();
    }
}