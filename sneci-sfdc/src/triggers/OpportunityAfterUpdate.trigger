trigger OpportunityAfterUpdate on Opportunity (after update) {
    system.debug('##OpportunityAfterUpdate Begin');
    AP01_oprApprovalManager.checkRejectedApproval(Trigger.newMap, Trigger.oldMap);
    AP04_EditBudget.checkPrerequisite(Trigger.new, Trigger.old);
    system.debug('##OpportunityAfterUpdate End');
}