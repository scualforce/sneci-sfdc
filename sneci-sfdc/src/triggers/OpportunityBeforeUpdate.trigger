trigger OpportunityBeforeUpdate on Opportunity (before Update) {
    system.debug('##OpportunityBeforeUpdate Begin');
    AP01_oprApprovalManager.SetInstruction(Trigger.newMap, Trigger.oldMap);
    system.debug('##OpportunityBeforeUpdate End');
}